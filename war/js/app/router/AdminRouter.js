var Router = Backbone.Router.extend({
    routes: {
        "": "homepage",
        "categories": "categories",
        "categories/detail/:id": "categoryDetail",
        "categories/new": "categoryNew",
        "e-shops": "eshops",
        "e-shops/detail/:id": "eshopDetail",
        "e-shops/new": "eshopNew",
        "e-shops/detail/:id/products": "eshopProducts",
        "e-shops/detail/:id/products/page/:page": "eshopProducts",
        "e-shops/detail/:eshopId/products/detail/:productId": "productDetail",
        "products": "products",
        "products/*path": "products"
    },
    params: {},
    homepage: function() {
        var homepage = new Homepage();
        homepage.render();
    },
    categories: function() {
        collections.categoryCollection.fetch();
        new CategoriesView().render();
        new CategoryList({collection: collections.categoryCollection}).render();
    },
    categoryDetail: function(id) {
        var category = new Category({id: id});
        if (this.categoryView) {
            this.categoryView.close();
        }
        this.categoryView = new CategoryDetail({model: category});
        category.fetch({reset: true});
    },
    categoryNew: function() {
        var category = new Category();
        if (this.categoryView) {
            this.categoryView.close();
        }
        this.categoryView = new CategoryDetail({model: category});
        this.categoryView.render();
    },
    eshops: function() {
        collections.eshopCollection.fetch();
        new EshopsView().render();
        new EshopTable({collection: collections.eshopCollection}).render();
    },
    eshopDetail: function(id) {
        var eshop = new Eshop({id: id});
        if (this.eshopView) {
            this.eshopView.close();
        }
        this.eshopView = new EshopDetail({model: eshop});
        eshop.fetch({reset: true});
    },
    eshopNew: function() {
        var eshop = new Eshop();
        if (this.eshopView) {
            this.eshopView.close();
        }
        this.eshopView = new EshopDetail({model: eshop});
        this.eshopView.render();
    },
    eshopProducts: function(id, page) {
        if (page === null) {
            page = 1;
        }
        var products = new ProductCollection();
        var eshop = new Eshop({id: id, products: products});
        var eshopProductListView = new EshopProductList({eshop: eshop});
        var paginator = new Paginator({collection: products, currentPage: page, baseUrl: "e-shops/detail/" + id + "/products"});
        eshop.fetch();
        products.fetch({reset: true});
    },
    productDetail: function(eshopId, productId) {
        collections.categoryCollection.fetch();
        var product = new Product({
            id: productId,
            eshop: {
                id: eshopId
            }
        });
        if (this.productView) {
            this.productView.close();
        }
        this.productView = new ProductDetail({model: product, categories: collections.categoryCollection});
        product.fetch();
    },
    products: function(path) {
        if (path !== null) {
            var params = $.deparam(path);
        } else {
            params = {};
        }
        if (params.page === undefined) {
            params.page = 1;
        }

        collections.categoryCollection.fetch();
        var products = new ProductCollection(params);
        new ProductsView({categories: collections.categoryCollection}).render();
        var paginator = new Paginator({collection: products, currentPage: params.page, baseUrl: "products"});

        params.collection = products;
        params.paginator = paginator;
        var productListView = new ProductList(params);

        products.fetch({reset: true});
    }
});

// Setup receiving and sending authentication token
var authToken;
$(document).ajaxSuccess(function(event, xhr, settings) {
    authToken = xhr.getResponseHeader(Settings.HEADER_TOKEN);
});
$(document).ajaxSend(function(event, xhr, settings) {
    if (authToken !== undefined && authToken !== null && authToken !== "null") {
        xhr.setRequestHeader(Settings.HEADER_TOKEN, authToken);
    }
});

// Load resources' URLs from single entry point
var resources = {};
$.ajax({
    url: "/api",
    type: "GET",
    success: function(data) {
        resources.category = data.category;
        resources.eshop = data.eshop;
        resources.product = data.product;
    },
    async: false
});

// Init basic collections
var collections = {};
collections.categoryCollection = new CategoryCollection();
collections.categoryCollection.comparator = "name";
collections.eshopCollection = new EshopCollection();
collections.eshopCollection.comparator = "name";

// Start application
var App = new Router();
Backbone.history.start();

