var Router = Backbone.Router.extend({
    routes: {
        "": "homepage",
        "products": "products",
        "products/*path": "products",
        "e-shops/detail/:eshopId/products/detail/:productId": "productDetail",
    },
    params: {},
    initialize: function() {
        this.header = new HeaderView();
        this.header.render();
    },
    homepage: function() {
        collections.categoryCollection.fetch();
        var categoriesView = new CategoriesView({collection: collections.categoryCollection});
        categoriesView.render();
        this.header.clearSearchField();
    },
    products: function(path) {
        if (path !== null) {
            var params = $.deparam(path);
        } else {
            params = {};
        }
        if (params.page === undefined) {
            params.page = 1;
        }
        if (params.search === undefined) {
            this.header.clearSearchField();
        } else {
            this.header.setSearchField(params.search);
        }

        collections.categoryCollection.fetch();
        var products = new ProductCollection(params);
      //  new ProductsView({categories: collections.categoryCollection}).render();
        var paginator = new Paginator({collection: products, currentPage: params.page, baseUrl: "products"});

        params.collection = products;
        params.paginator = paginator;
        params.categories = collections.categoryCollection;
        var productListView = new ProductList(params);

        products.fetch({reset: true});
    },
    productDetail: function(eshopId, productId) {
        collections.categoryCollection.fetch();
        var product = new Product({
            id: productId,
            eshop: {
                id: eshopId
            }
        });
        if (this.productView) {
            this.productView.close();
        }
        this.productView = new ProductDetail({model: product, categories: collections.categoryCollection});
        product.fetch();
    }
});

// Setup receiving and sending authentication token
var authToken;
$(document).ajaxSuccess(function(event, xhr, settings) {
    authToken = xhr.getResponseHeader(Settings.HEADER_TOKEN);
});
$(document).ajaxSend(function(event, xhr, settings) {
    if (authToken !== undefined && authToken !== null && authToken !== "null") {
        xhr.setRequestHeader(Settings.HEADER_TOKEN, authToken);
    }
});

// Load resources' URLs from single entry point
var resources = {};
$.ajax({
    url: "/api",
    type: "GET",
    success: function(data) {
        resources.category = data.category;
        resources.eshop = data.eshop;
        resources.product = data.product;
    },
    async: false
});

// Init basic collections
var collections = {};
collections.categoryCollection = new CategoryCollection();
collections.categoryCollection.comparator = "name";
collections.eshopCollection = new EshopCollection();
collections.eshopCollection.comparator = "name";

// Start application
var App = new Router();
Backbone.history.start();

