var CategoriesView = Backbone.View.extend({
    template: _.template($('#tmpl-categories').html()),
    initialize: function() {
        this.listenTo(this.collection, 'change', this.render);
        this.listenTo(this.collection, 'add', this.render);
        this.listenTo(this.collection, 'remove', this.render);
    },
    render: function() {
        $(this.el).html(this.template({categories: this.collection}));
        $('#content').html($(this.el));
        $('#breadcrumbs-panel').hide();
        return this;
    }

});

