var ProductList = Backbone.View.extend({
    events: {
        "submit .filter-form": "filter"
    },
    template: _.template($('#tmpl-products').html()),
    initialize: function(params) {
        this.categoryID = params.category;
        this.categories = params.categories;
        this.paginator = params.paginator;
        this.listenTo(this.collection, 'reset', this.render);
        this.listenTo(this.categories, 'add', this.render);
    },
    breadcrumbs: function() {
        $('#breadcrumbs-panel').show();
        var category;
        if(this.categoryID !== undefined) {
            category = this.categories.get(this.categoryID);
        }
        var breadcrumbString = category !== undefined ? category.get('name') : "Produkty";
        
        return '<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>\n\
                <li class="active">' + breadcrumbString + '</li>';
    },
    render: function() {
        $(this.el).html(this.template({products: this.collection, categories: this.categories}));
        $('#content').html($(this.el));

        $('.breadcrumb').html(this.breadcrumbs()).show();

        $(".filter-form").submit(this.filter.bind(this));

        if (this.categoryID) {
            $("#category-select").val(this.categoryID);
        }
        if (this.priceFrom) {
            $("#price-from").val(this.priceFrom);
        }
        if (this.priceTo) {
            $("#price-to").val(this.priceTo);
        }

        return this;
    },
    filter: function() {
        this.categoryID = $("#category-select").val();
        if (this.categoryID !== "") {
            this.collection.category = this.categoryID;
            App.params.category = this.categoryID;
        } else {
            delete this.collection.category;
            delete App.params.category;
        }

        this.priceFrom = $("#price-from").val();
        if (this.priceFrom !== "") {
            this.collection.priceFrom = this.priceFrom;
            App.params.priceFrom = this.priceFrom;
        } else {
            delete this.collection.priceFrom;
            delete App.params.priceFrom;
        }

        this.priceTo = $("#price-to").val();
        if (this.priceTo !== "") {
            this.collection.priceTo = this.priceTo;
            App.params.priceTo = this.priceTo;
        } else {
            delete this.collection.priceTo;
            delete App.params.priceTo;
        }

        if (App.header) {
            App.header.clearSearchField();
        }
        delete this.collection.search;

        this.paginator.reset();
        return false;
    }
});

