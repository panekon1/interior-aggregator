var HeaderView = Backbone.View.extend({
    events: {
        "submit #search-form": "search"
    },
    template: _.template($('#tmpl-header').html()),
    render: function() {
        $(this.el).html(this.template());
        $('#header').html($(this.el));
        return this;
    },
    search: function() {
        var searchString = $("#search-form").find('input[type=search]').val();

        if (searchString.trim() !== '') {
            App.navigate("products?search=" + searchString, {trigger: true});
        }

        return false;
    },
    clearSearchField: function() {
        $("#search-form").find('input[type=search]').val("");
    },
    setSearchField: function(searchString) {
        $("#search-form").find('input[type=search]').val(searchString);
    }

});

