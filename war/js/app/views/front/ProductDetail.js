var ProductDetail = Backbone.View.extend({
    events: {
    },
    template: _.template($('#tmpl-product-detail').html()),
    initialize: function(options) {
        this.listenTo(this.model, 'change', this.render);
    },
    breadcrumbs: function() {
        $('#breadcrumbs-panel').show();
        var middle;
        if (this.model.get('category') !== null) {
            middle = '<li><a href="#products/category=' + this.model.get('category').id + '">' + this.model.get('category').name + '</a></li>';
        } else {
            middle = '<li><a href="#products">Produkty</a></li>';
        }
        
        return '<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>\n\
                ' + middle + '\
                <li class="active">' + this.model.get('name') + '</li>';
    },
    render: function() {
        if (this.model !== null && this.categories !== null && this.model.get('similarProducts').length !== undefined) {
            $(this.el).html(this.template({product: this.model.toJSON()}));

            $('.breadcrumb').html(this.breadcrumbs());
            $('#content').html($(this.el));
        }
        return this;
    },
    close: function() {
        $(this.el).unbind();
        $(this.el).empty();
    }
});

