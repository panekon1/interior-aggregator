var Paginator = Backbone.View.extend({
    events: {
        "click .page a": "jumpTo",
        "click .next a": "next",
        "click .prev a": "prev",
    },
    template: _.template($('#tmpl-paginator').html()),
    initialize: function(parameters) {
        this.currentPage = (parameters !== undefined && parameters.currentPage !== undefined) ? parseInt(parameters.currentPage) : 1;
        this.itemsPerPage = Settings.ITEMS_PER_PAGE;
        this.firstPage = 1;

        this.baseUrl = parameters.baseUrl;
        this.silent = parameters.silent !== undefined &&  parameters.silent;

        this.collection.base = this.base();
        this.collection.offset = this.offset();
        this.listenTo(this.collection, 'itemsCountLoaded', this.updateTotalItems);
    },
    render: function() {
        if(this.totalPages < this.currentPage) {
            this.jumpTo(this.lastPage);
        }
        
        $(this.el).html(this.template(this.renderParams()));
        if (this.totalPages !== 1) {
            $(".paginator").html($(this.el));

            $(".page a").click(this.jumpTo.bind(this));
            $(".prev a").click(this.prev.bind(this));
            $(".next a").click(this.next.bind(this));

            if (this.currentPage === this.firstPage) {
                $(".prev").addClass("disabled");
            }
            if (this.currentPage === this.lastPage) {
                $(".next").addClass("disabled");
            }
        }
        return this;
    },
    renderParams: function() {
        return {
            currentPage: this.currentPage,
            totalPages: this.totalPages,
            baseUrl: this.baseUrl
        };
    },
    base: function() {
        return (this.currentPage - 1) * this.itemsPerPage;
    },
    offset: function() {
        return this.itemsPerPage;
    },
    next: function() {
        this.currentPage = this.currentPage < this.lastPage ? this.currentPage + 1 : this.lastPage;
        this.updatePagination();
        return false;
    },
    prev: function() {
        this.currentPage = this.currentPage > this.firstPage ? this.currentPage - 1 : 1;
        this.updatePagination();
        return false;
    },
    jumpTo: function(event) {
        event.preventDefault();
        this.currentPage = parseInt($(event.target).attr("data-page"));
        this.updatePagination();
        return false;
    },
    reset: function() {
        this.currentPage = 1;
        this.updatePagination();
    },
    updatePagination: function() {
        this.collection.base = this.base();
        this.collection.offset = this.offset();
        this.collection.fetch({reset: true});
        
        if(!this.silent) {
            if (this.currentPage !== 1) {
                App.params.page = this.currentPage;
            } else {
                delete App.params.page;
            }
            App.navigate(this.baseUrl + "/" + $.param(App.params));
        }
    },
    updateTotalItems: function() {
        this.totalItems = this.collection.totalItems;
        this.lastPage = Math.max(1, parseInt(this.totalItems / this.itemsPerPage));
        this.totalPages = this.lastPage;
        this.render();
    }
});

