var ProductDetail = Backbone.View.extend({
    events: {
        "click .save-product": "saveProduct",
        "click .delete-product": "deleteProduct",
        "click .unlink": "unlinkProduct"
    },
    template: _.template($('#tmpl-product-detail').html()),
    initialize: function(options) {
        this.categories = options.categories;
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.categories, 'add', this.render);
    },
    breadcrumbs: function() {
        return '<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>\n\
         <li><a href="#e-shops">E-shopy</a></li>\n\
         <li><a href="#e-shops/detail/' + this.model.get('eshop').id + '">' + this.model.get('eshop').name + '</a></li>\n\
         <li><a href="#e-shops/detail/' + this.model.get('eshop').id + '/products">Produkty</a></li>\n\
         <li class="active">' + this.model.get('name') + '</li>';
    },
    render: function() {
        if (this.model !== null && this.categories !== null && this.model.get('similarProducts').length !== undefined) {
            $(this.el).html(this.template({product: this.model.toJSON(), categories: this.categories}));

            $('.nav-pills li').removeClass('active');
            $('.nav-pills .eshops-pill').addClass('active');
            $('.breadcrumb').html(this.breadcrumbs()).show();
            $('.page-header').html(this.model.get('name')).show();
            $('#content').html($(this.el));

            if (this.model.get("category") !== null) {
                $(this.el).find("#product-category").val(this.model.get("category").id);
            }
            $(this.el).find(".save-product").click(this.saveProduct.bind(this));
            $(this.el).find(".delete-product").click(this.deleteProduct.bind(this));
            $(this.el).find(".unlink").click(this.unlinkProduct.bind(this));
            $(this.el).find(".open-modal").click(this.openModal.bind(this));
        }
        return this;
    },
    close: function() {
        $(this.el).unbind();
        $(this.el).empty();
    },
    openModal: function() {
        if(this.modal === undefined) {
            var products = new ProductCollection();
            var paginator = new Paginator({collection: products, currentPage: 1, silent:true});

            var productListView = new ProductList({
                collection: products,
                paginator: paginator,
                type: 'select'
            });

            this.modal = new ProductLinkModal({
                model: this.model,
                categories: this.categories,
                listView: productListView,
                paginator: paginator
            });

            products.fetch({reset: true});
            
            this.listenTo(this.modal, "productsLinked", this.render);
        }
        
        this.modal.render();
    },
    saveProduct: function() {
        var categoryId = $("#product-category").val();
        if (categoryId !== "") {
            var category = this.categories.get(categoryId).toJSON();
        } else {
            var category = null;
        }

        this.model.set({
            name: $("#product-name").val(),
            description: $("#product-description").val(),
            url: $("#product-url").val(),
            price: $("#product-price").val(),
            priceWithVAT: $("#product-price-with-vat").val(),
            imageURL: $("#product-image-url").val(),
            category: category
        });
        this.model.save();
        App.navigate("e-shops/detail/" + this.model.get("eshop").id + "/products", {trigger: true});

        return false;
    },
    deleteProduct: function() {
        var route = "e-shops/detail/" + this.model.get("eshop").id + "/products";
        this.model.destroy({
            success: function() {
                App.navigate(route, {trigger: true});
            }
        });
        return false;
    },
    unlinkProduct: function(event) {
        var $target = $(event.target);

        // Send request to unlink 
        this.model.unlink($target.attr('data-eshop'), $target.attr('data-product'));

        // Remove the product from collection of similar products
        $.each(this.model.get('similarProducts'), function(i, item) {
            if (item.id === parseInt($target.attr('data-product'))) {
                this.model.get('similarProducts').splice(i);
            }
        }.bind(this));

        this.render();
        return false;
    }
});

