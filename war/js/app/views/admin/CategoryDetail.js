var CategoryDetail = Backbone.View.extend({
    events: {
        "click .save-category": "saveCategory",
        "click .delete-category": "deleteCategory"
    },
    template: _.template($('#tmpl-category-detail').html()),
    initialize: function() {
        this.listenTo(this.model, 'change', this.render);
    },
    breadcrumbs: function() {
        var base = '<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>\n\
                    <li><a href="#categories">Kategorie</a></li>';
        if (this.model.isNew()) {
            return base + '<li class="active">Vytvořit</li>';
        } else {
            return base + '<li class="active">' + this.model.get('name') + '</li>';
        }
    },
    render: function() {
        $(this.el).html(this.template(this.model.toJSON()));
        if (this.model.isNew()) {
            $(this.el).find(".delete-category").hide();
        }
        $('.nav-pills li').removeClass('active');
        $('.nav-pills .categories-pill').addClass('active');
        $('.breadcrumb').html(this.breadcrumbs()).show();
        $('.page-header').html("Kategorie " + this.model.get('name')).show();
        $('#content').html($(this.el));
        return this;
    },
    close: function() {
        $(this.el).unbind();
        $(this.el).empty();
    },
    saveCategory: function() {
        this.model.set({
            name: $('#category-name').val()
        });
        this.model.save();
        if (this.model.validationError) {
            alert(this.model.validationError);
        } else {
            App.navigate("categories", {trigger: true});
        }
                
        return false;
    },
    deleteCategory: function() {
        this.model.destroy({
            success: function() {
                collections.categoryCollection.remove(this.model);
                App.navigate("categories", {trigger: true});
            }
        });
        return false;
    },
});

