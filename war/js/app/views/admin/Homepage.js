var Homepage = Backbone.View.extend({
    template: _.template($('#tmpl-homepage').html()),
    render: function() {
        $(this.el).html(this.template());
        $('.nav-pills li').removeClass('active');
        $('.nav-pills .homepage-pill').addClass('active');
        $('.breadcrumb').hide();
        $('.page-header').hide();
        $('#content').html($(this.el));
        return this;
    },
});


