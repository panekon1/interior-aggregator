var ProductsView = Backbone.View.extend({
    events: {
    },
    template: _.template($('#tmpl-products').html()),
    initialize: function(options) {
        this.categories = options.categories;
        this.listenTo(this.categories, 'add', this.render);
    },
    breadcrumbs: function() {
        return '<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>\n\
                <li class="active">Produkty</li>';
    },
    render: function() {
        $(this.el).html(this.template({categories: this.categories}));
        $('.nav-pills li').removeClass('active');
        $('.nav-pills .products-pill').addClass('active');
        $('.breadcrumb').html(this.breadcrumbs()).show();
        $('.page-header').html("Produkty").show();
        $('#content').html($(this.el));
        
        return this;
    }

});

