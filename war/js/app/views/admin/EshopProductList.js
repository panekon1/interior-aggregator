var EshopProductList = Backbone.View.extend({
    events: {
    },
    template: _.template($('#tmpl-product-list').html()),
    initialize: function(attributes) {
        this.eshop = attributes.eshop;
        this.listenTo(this.eshop.products, 'reset', this.render);
    },
    breadcrumbs: function() {
        return '<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>\n\
                <li><a href="#e-shops">E-shopy</a></li>\n\
                <li><a href="#e-shops/detail/' + this.eshop.get('id') + '">' + this.eshop.get('name') + '</a></li>\n\
                <li class="active">Seznam produktů</li>';
    },
    render: function() {
        $(this.el).html(this.template({products: this.eshop.get("products")}));
        $('.nav-pills li').removeClass('active');
        $('.nav-pills .eshops-pill').addClass('active');
        $('.breadcrumb').html(this.breadcrumbs()).show();
        $('.page-header').html("E-shop " + this.eshop.get('name') + ' - seznam produktů').show();
        $('#content').html($(this.el));
        
        this.trigger("rendered");
        
        return this;
    },
    newEshop: function() {
        App.navigate("e-shops/new", {trigger: true});
    }

});

