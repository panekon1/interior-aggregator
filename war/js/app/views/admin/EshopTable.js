var EshopTable = Backbone.View.extend({
    tagName: "table",
    className: "table table-striped",
    template: _.template($('#tmpl-eshop-table').html()),
    initialize: function() {
        this.listenTo(this.collection, 'change', this.render);
        this.listenTo(this.collection, 'add', this.render);
        this.listenTo(this.collection, 'remove', this.render);
    },
    render: function() {
        $(this.el).html(this.template({eshops: this.collection}));
        $('#eshops').html($(this.el));
        return this;
    },
});

