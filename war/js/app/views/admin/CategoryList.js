var CategoryList = Backbone.View.extend({
    tagName: "table",
    className: "table table-striped",
    template: _.template($('#tmpl-category-list').html()),
    initialize: function() {
        this.listenTo(this.collection, 'change', this.render);
        this.listenTo(this.collection, 'add', this.render);
        this.listenTo(this.collection, 'remove', this.render);
    },
    render: function() {
        $(this.el).html(this.template({categories: this.collection}));
        $('#categories').html($(this.el));
        return this;
    },
    newCategory: function() {
        App.navigate("categories/new", {trigger: true});
    }

});

