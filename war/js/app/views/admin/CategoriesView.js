var CategoriesView = Backbone.View.extend({
    events: {
        "click .new-category": "newCategory"
    },
    template: _.template($('#tmpl-categories').html()),
    breadcrumbs: function() {
        return '<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>\n\
                <li class="active">Kategorie</li>';
    },
    render: function() {
        $(this.el).html(this.template());
        $('.nav-pills li').removeClass('active');
        $('.nav-pills .categories-pill').addClass('active');
        $('.breadcrumb').html(this.breadcrumbs()).show();
        $('.page-header').html("Kategorie").show();
        $('#content').html($(this.el));
        return this;
    },
    newCategory: function() {
        App.navigate("categories/new", {trigger: true});
    }

});

