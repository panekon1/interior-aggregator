var ProductList = Backbone.View.extend({
    events: {
        "submit .filter-form": "filter"
    },
    template: null,
    initialize: function(params) {
        if (params.category !== undefined) {
            this.category = params.category;
        }
        this.paginator = params.paginator;
        this.template = params.type === 'select' ? _.template($('#tmpl-product-select').html()) : _.template($('#tmpl-product-list').html());
        this.listenTo(this.collection, 'reset', this.render);
    },
    render: function() {
        $(this.el).html(this.template({products: this.collection}));
        $('#products').html($(this.el));

        $(".filter-form").submit(this.filter.bind(this));

        if (this.category) {
            $("#category-select").val(this.category);
        }
        if (this.priceFrom) {
            $("#price-from").val(this.priceFrom);
        }
        if (this.priceTo) {
            $("#price-to").val(this.priceTo);
        }

        return this;
    },
    filter: function() {
        this.category = $("#category-select").val();
        if (this.category !== "") {
            this.collection.category = this.category;
            App.params.category = this.category;
        } else {
            delete this.collection.category;
            delete App.params.category;
        }

        this.priceFrom = $("#price-from").val();
        if (this.priceFrom !== "") {
            this.collection.priceFrom = this.priceFrom;
            App.params.priceFrom = this.priceFrom;
        } else {
            delete this.collection.priceFrom;
            delete App.params.priceFrom;
        }

        this.priceTo = $("#price-to").val();
        if (this.priceTo !== "") {
            this.collection.priceTo = this.priceTo;
            App.params.priceTo = this.priceTo;
        } else {
            delete this.collection.priceTo;
            delete App.params.priceTo;
        }

        if (App.header) {
            App.header.clearSearchField();
        }
        delete this.collection.search;

        this.paginator.reset();
        return false;
    }
});

