var EshopsView = Backbone.View.extend({
    events: {
        "click .new-eshop": "newEshop"
    },
    template: _.template($('#tmpl-eshops').html()),
    initialize: function() {
    },
    breadcrumbs: function() {
        return '<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>\n\
                <li class="active">E-shopy</li>';
    },
    render: function() {
        $(this.el).html(this.template());
        $('.nav-pills li').removeClass('active');
        $('.nav-pills .eshops-pill').addClass('active');
        $('.breadcrumb').html(this.breadcrumbs()).show();
        $('.page-header').html("E-shopy").show();
        $('#content').html($(this.el));
        return this;
    },
    newEshop: function() {
        App.navigate("e-shops/new", {trigger: true});
    }

});

