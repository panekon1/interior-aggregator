var EshopDetail = Backbone.View.extend({
    events: {
        "click .save-eshop": "saveEshop",
        "click .delete-eshop": "deleteEshop"
    },
    template: _.template($('#tmpl-eshop-detail').html()),
    initialize: function() {
        this.listenTo(this.model, 'change', this.render);
    },
    breadcrumbs: function() {
        var base = '<li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>\n\
                    <li><a href="#e-shops">E-shopy</a></li>';
        if (this.model.isNew()) {
            return base + '<li class="active">Vytvořit</li>';
        } else {
            return base + '<li class="active">' + this.model.get('name') + '</li>';
        }
    },
    render: function() {
        $(this.el).html(this.template(this.model.toJSON()));
        if (this.model.isNew()) {
            $(this.el).find(".delete-eshop").hide();
        }
        $('.nav-pills li').removeClass('active');
        $('.nav-pills .eshops-pill').addClass('active');
        $('.breadcrumb').html(this.breadcrumbs()).show();
        $('.page-header').html("E-shop " + this.model.get('name')).show();
        $('#content').html($(this.el));
        return this;
    },
    close: function() {
        $(this.el).unbind();
        $(this.el).empty();
    },
    saveEshop: function() {
        this.model.set({
            name: $('#eshop-name').val(),
            web: $('#eshop-web').val(),
            feed: $('#eshop-feed').val()
        });
        this.model.save();
        if (this.model.validationError) {
            alert(this.model.validationError);
        } else {
            App.navigate("e-shops", {trigger: true});
        }
                
        return false;
    },
    deleteEshop: function() {
        this.model.destroy({
            success: function() {
                collections.eshopCollection.remove(this.model);
                App.navigate("e-shops", {trigger: true});
            }
        });
        return false;
    },
});

