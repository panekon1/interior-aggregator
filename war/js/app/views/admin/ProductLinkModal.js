var ProductLinkModal = Backbone.View.extend({
    events: {
        "change #product-select": "updateSelection"
    },
    template: _.template($('#tmpl-product-modal').html()),
    initialize: function(options) {
        this.categories = options.categories;
        this.paginator = options.paginator;
        this.listView = options.listView;
    },
    render: function() {
        $(this.el).html(this.template({product: this.model.toJSON(), categories: this.categories}));
        $("#content").append($(this.el));

        $(this.el).find('#productModal').modal('show');
        this.listView.render();
        this.paginator.render();
        $("#product-select").change(this.updateSelection);
        $(".link-button").click(this.linkProduct.bind(this));

        this.updateSelection();
        return this;
    },
    updateSelection: function() {
        if ($('#product-select').val() === undefined || $('#product-select').val() === null) {
            $(".link-button").attr("disabled", "disabled");
        } else {
            $(".link-button").removeAttr("disabled");
        }
    },
    linkProduct: function(event) {
        if ($('#product-select').val() !== undefined && $('#product-select').val() !== null) {
            var productID = $('#product-select').val();
            var eshopID = $('#product-select option:selected').attr("data-eshop");
            
            this.model.link(eshopID, productID);
            
            var product = this.listView.collection.get(productID);
            this.model.get("similarProducts").push(product.toJSON());
            this.trigger("productsLinked");
          
            this.hide();
        }
    },
    hide: function() {
        $(this.el).find('#productModal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }
});