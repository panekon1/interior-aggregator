var Settings = {
    ITEMS_PER_PAGE: 20,
    HEADER_TOKEN: "X-Authentication-Token"
};

_.templateSettings = {
    evaluate: /\{\{(.+?)\}\}/g,
    interpolate: /\{\{=(.+?)\}\}/g,
    escape: /\{\{-(.+?)\}\}/g
};