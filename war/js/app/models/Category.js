var Category = Backbone.Model.extend({
    urlRoot: function() {
        return resources.category;
    },
    defaults: {
        "id": null,
        "name": "",
    },
    validate: function(attrs, options) {
        if (attrs.name.trim().length === 0) {
            return "Vyplňte prosím název kategorie.";
        }
    }
});

var CategoryCollection = Backbone.Collection.extend({
    model: Category,
    url: function() {
        return resources.category;
    }
});