var Product = Backbone.Model.extend({
    urlRoot: function() {
        return resources.eshop + "/" + this.get('eshop').id + "/product";
    },
    defaults: {
        "id": null,
        "name": "",
        "description": "",
        "url": "",
        "price": "",
        "priceWithVAT": "",
        "imageURL": "",
        "eshop": {
            "id": null
        },
        "category": {
            "id": null
        },
        "similarProducts": {}
    },
    validate: function(attrs, options) {
        if (attrs.name.trim().length === 0) {
            return "Vyplňte prosím název produktu";
        }
    },
    link: function(eshopID, productID) {
        $.post(this.url() + "/link/" + eshopID + "/" + productID);
    },
    unlink: function(eshopID, productID) {
        $.post(this.url() + "/unlink/" + eshopID + "/" + productID);
    }
});

var ProductCollection = Backbone.Collection.extend({
    model: Product,
    url: function() {
        return resources.product;
    },
    initialize: function(parameters) {
        if (parameters !== undefined) {
            this.category = parameters.category;
            this.search = parameters.search;
            this.priceFrom = parameters.priceFrom;
            this.priceTo = parameters.priceTo;
        }
        // Default pagination params (should be eventually overriden by paginator)
        this.base = 0;
        this.offset = Settings.ITEMS_PER_PAGE;
    },
    fetch: function(options) {
        // Pagination
        options.beforeSend = function(xhr) {
            xhr.setRequestHeader('X-Base', this.base);
            xhr.setRequestHeader('X-Offset', this.offset);
            if (this.category !== undefined) {
                xhr.setRequestHeader('X-Category', this.category);
            }
            if (this.priceFrom !== undefined) {
                xhr.setRequestHeader('X-Price-From', this.priceFrom);
            }
            if (this.priceTo !== undefined) {
                xhr.setRequestHeader('X-Price-To', this.priceTo);
            }
            if (this.search !== undefined) {
                xhr.setRequestHeader('X-Search', this.search);
            }
        }.bind(this);

        options.success = function(collection, response, options) {
            this.totalItems = options.xhr.getResponseHeader("X-Count");
            this.trigger("itemsCountLoaded");
        }.bind(this);

        // Call Backbone's fetch
        return Backbone.Collection.prototype.fetch.call(this, options);
    }
});
