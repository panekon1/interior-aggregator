var Eshop = Backbone.Model.extend({
    urlRoot: function() {
        return resources.eshop;
    },
    defaults: {
        "id": null,
        "name": "",
        "web": "",
        "feed": ""
    },
    initialize: function(attributes) {
        var self = this;
        if (attributes !== undefined && attributes.products !== undefined) {
            this.products = attributes.products;
            this.products.url = function() {
                return self.url() + '/product';
            };
        }
    },
    validate: function(attrs, options) {
        if (attrs.name.trim().length === 0) {
            return "Vyplňte prosím název e-shopu.";
        }
    }
});

var EshopCollection = Backbone.Collection.extend({
    model: Eshop,
    url: function() {
        return resources.eshop;
    },
    initialize: function() {
        this.listenTo(this, 'add', this.sort);
    }
});