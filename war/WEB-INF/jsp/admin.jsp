<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored ="true" %>
<html>
    <head lang="cs">
        <title class="title">Administrace Interiéry.cz</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

        <!-- CSS -->
        <link rel="stylesheet" href="/css/styles.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="admin">

        <header id="header">
            <div class="container">
                <h1>Administrace</h1>
                <ul class="nav nav-pills">
                    <li class="homepage-pill"><a href="#">Domů</a></li>
                    <li class="categories-pill"><a href="#categories">Kategorie</a></li>
                    <li class="eshops-pill"><a href="#e-shops">E-shopy</a></li>
                    <li class="products-pill"><a href="#products">Produkty</a></li>
                </ul>
                <small id="admin-link"><a href="/">Zpět do veřejné části</a></small>  
            </div>
        </header>
        
        <div id="breadcrumbs-panel">
            <div class="container">
                <ol class="breadcrumb"></ol>
            </div>
        </div>

        <div id="wrapper" role="main" class="container">

            <h2 class="page-header"></h2>

            <div id="content"></div>

        </div>

        <!-- Templates -->
        <script type="text/x-jquery-tmpl" id="tmpl-homepage">

        </script>

        <script type="text/x-jquery-tmpl" id="tmpl-categories">
            <button type="button" class="btn btn-primary new-category">Přidat kategorii</button>
            <div id="categories"></div>
        </script>
        
        <script type="text/x-jquery-tmpl" id="tmpl-category-list">
            <thead>
                <tr>
                    <th>Název</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                {{ categories.each(function(model){ }}
                    {{ var category = model.toJSON(); }}
                    <tr>
                        <td><a href='#categories/detail/{{= category.id }}'>{{= category.name }}</a></td>
                        <td><a href="#products/category={{= category.id }}">Seznam produktů</a></td>
                    </tr>
                {{ }); }}
            </tbody>
        </script>

        <script type="text/x-jquery-tmpl" id="tmpl-category-detail">
            <form role="form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="category-name">Název kategorie</label>
                            <input type="text" class="form-control" id="category-name" name="category-name" value="{{= name }}" placeholder="např. Křesla" required />
                        </div>
                    </div>
                </div>

                <button type="button" class="btn btn-success save-category">Uložit</button>
                <button type="button" class="btn btn-danger delete-category">Smazat</button>
            </form>
        </script>

        <script type="text/x-jquery-tmpl" id="tmpl-eshops">
            <button type="button" class="btn btn-primary new-eshop">Přidat e-shop</button>
            <div id="eshops"></div>
        </script>  
        
        <script type="text/x-jquery-tmpl" id="tmpl-eshop-table">
            <thead>
                <tr>
                    <th>Název</th>
                    <th>Web</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                {{ eshops.each(function(model){ }}
                    {{ var eshop = model.toJSON(); }}
                    <tr>
                        <td><a href='#e-shops/detail/{{= eshop.id }}'>{{= eshop.name }}</a></td>
                        <td><a href='{{= eshop.web }}'>{{= eshop.web }}</a></td>
                        <td><a href='#e-shops/detail/{{= eshop.id }}/products'>Seznam produktů</a></td>
                    </tr>
                {{ }); }}
            </tbody>
        </script>        

        <script type="text/x-jquery-tmpl" id="tmpl-eshop-detail">
            <form role="form">
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="eshop-name">Název</label>
                            <input type="text" class="form-control" id="eshop-name" name="eshop-name" value="{{= name }}" placeholder="Můj E-shop" required />
                        </div>
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="eshop-web">Web</label>
                            <input type="text" class="form-control" id="eshop-web" name="eshop-web" value="{{= web }}" placeholder="http://www.e-shop.cz" />
                        </div>
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="eshop-feed">XML feed</label>
                            <input type="text" class="form-control" id="eshop-feed" name="eshop-feed" value="{{= feed }}" placeholder="http://www.e-shop.cz/feed.xml" />
                        </div>
                    </div>
                </div>

                <button type="button" class="btn btn-success save-eshop">Uložit</button>
                <button type="button" class="btn btn-danger delete-eshop">Smazat</button>
            </form>
        </script>
        
        <script type="text/x-jquery-tmpl" id="tmpl-products">
            <form class="form-inline filter-form" role="form">
                <div class="form-group">
                    <label for="category-select">Kategorie:</label>
                    <select class="form-control" id="category-select">
                        <option value>Všechny kategorie</option>
                        {{ categories.each(function(model){ }}
                            {{ var category = model.toJSON(); }}
                            <option value="{{= category.id }}">{{= category.name }}</option>
                        {{ }); }}
                    </select>
                </div>
                <div class="form-group">
                    <label for="price-from">Cena od:</label>
                    <input type="number" class="form-control" id="price-from"> Kč
                </div>
                <div class="form-group">
                    <label for="price-to">Cena do:</label>
                    <input type="number" class="form-control" id="price-to"> Kč
                </div>
                <button type="submit" class="btn btn-default">Filtrovat</button>
            </form>
            
            <div id="products"></div>
        </script>

        <script type="text/x-jquery-tmpl" id="tmpl-product-list">
            <ul class="products-list">
                {{ products.each(function(model){ }}
                    {{ var product = model.toJSON(); }}
                    <li>
                        <article>
                            <div class="image"><span class="helper"></span><img src="{{= product.imageURL }}" alt="{{= product.name }}" class="product-image" /></div>
                            <div class="text">
                                <h3><a href='#e-shops/detail/{{= product.eshop.id }}/products/detail/{{= product.id }}'>{{= product.name }}</a></h3>
                                <p>{{= product.description }}</p>
                            </div>
                            <div class="price"><strong>{{= product.priceWithVAT }}</strong> Kč</div>
                            <a class="item-link" href='#e-shops/detail/{{= product.eshop.id }}/products/detail/{{= product.id }}'></a>
                        </article>
                    </li>
                {{ }); }}
            </ul>
            <div class="paginator"></div>
        </script>   
        
        <script type="text/x-jquery-tmpl" id="tmpl-product-select">
            {{ if(products.length === 0) { }}
                <p class="text-center">Nebyl nalezen žádný produkt</p>
            {{ } else { }}
                <form role="form">
                    <div class="form-group select">
                        <label for="product-select">Vyberte příbuzný produkt</label>
                        <div class="controls">
                                <select name="product-select" id="product-select" class="select optional" size="20">
                                {{ products.each(function(model){ }}
                                    {{ var product = model.toJSON(); }}
                                    <option value="{{= product.id }}" data-eshop="{{= product.eshop.id }}">{{= product.name }}</a></option>                   
                                {{ }); }}
                            </select>
                        </div>
                    </div>
                </form>
                <div class="paginator"></div>
            {{ } }}
        </script>  
        
        <script type="text/x-jquery-tmpl" id="tmpl-product-detail">
            <form role="form" class="product-form">        
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="product-name">Název</label>
                            <input type="text" class="form-control" id="product-name" name="product-name" value="{{= product.name }}" required />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="product-category">Kategorie</label> <small>(<a href="/#categories/new">vytvořit novou kategorii</a>)</small>
                            <select class="form-control" id="product-category" name="product-category">
                                <option value>-- Vyberte kategorii --</option>
                                {{ categories.each(function(model){ }}
                                    {{ var category = model.toJSON(); }}
                                    <option value="{{= category.id }}">{{= category.name }}</option>
                                {{ }); }}
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="product-web">Popis</label>
                    <textarea class="form-control" id="product-description" name="product-description" rows="3">{{= product.description }}</textarea>
                </div>
        
                <div class="form-group">
                    <label for="product-url">URL</label>
                    <input type="text" class="form-control" id="product-url" name="product-url" value="{{= product.url }}"/>
                </div>
        
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="product-price">Cena bez DPH</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="product-price" name="product-price" value="{{= product.price }}"/>
                                <span class="input-group-addon">,- Kč</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="product-price-with-vat">Cena s DPH</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="product-price-with-vat" name="product-price-with-vat" value="{{= product.priceWithVAT }}"/>
                                <span class="input-group-addon">,- Kč</span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="product-image-url">URL obrázku</label>
                    <input type="text" class="form-control" id="product-image-url" name="product-image-url" value="{{= product.imageURL }}"/>
                </div>
                <div class="form-group">
                    <label>Současný obrázek</label>
                    <div><a href="{{= product.imageURL }}"><img src="{{= product.imageURL }}" alt="{{= product.name }}" class="img-thumbnail product-image" /></a></div>
                </div>     
        
                <div class="form-group">
                    <label for="product-category">Příbuzné produkty</label>
                    <div><button type="button" class="btn btn-primary open-modal">Přidat produkt</button></div>
                    {{ if(product.similarProducts.length > 0){ }}
                        <ul class="similar-products-list">
                        {{ $.each(product.similarProducts, function(i, item) { }}
                            <li>
                                <a href='#e-shops/detail/{{= item.eshop.id }}/products/detail/{{= item.id }}'>{{= item.name }}</a>
                                <button data-eshop="{{= item.eshop.id }}" data-product="{{= item.id }}" class="unlink close" aria-hidden="true">&times;</button>
                            </li>
                        {{});}}
                        </ul>
                    {{}}}
                </div>

                <button type="button" class="btn btn-success save-product">Uložit</button>
                <button type="button" class="btn btn-danger delete-product">Smazat</button>
            </form>
        </script>
        
        <script type="text/x-jquery-tmpl" id="tmpl-product-modal">
            <!-- Modal -->
            <div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" aria-hidden="true" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Propojení produktu {{= product.name }}</h4>
                        </div>
                        <div class="modal-body">
                            <select class="form-control" id="category-select">
                                <option value>-- Vyberte kategorii --</option>
                                {{ categories.each(function(model){ }}
                                    {{ var category = model.toJSON(); }}
                                    <option value="{{= category.id }}">{{= category.name }}</option>
                                {{ }); }}
                            </select>
                            <div id="products"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
                            <button type="button" class="btn btn-primary link-button">Propojit</button>
                        </div>
                    </div>
                </div>
            </div>
        </script>
        
        <script type="text/x-jquery-tmpl" id="tmpl-paginator">
            <ul class="pagination">
                <li class="prev"><a href="">&laquo;</a></li>
                {{ for(p=1;p<=totalPages;p++){ }}
                    {{ if (currentPage == p) { }}
                      <li class="active page"><a href="" data-page="{{= p }}">{{= p }} <span class="sr-only">(current)</span></a></li>
                    {{ } else { }}
                      <li class="page"><a href="" data-page="{{= p }}">{{= p }}</a></li>
                    {{ } }}
                {{}}}
                <li class="next"><a href="">&raquo;</a></li>
            </ul>
        </script>

        

        <!-- jQuery -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.11.0.js"></script>
        <script src="/js/jquery/jquery.browser.min.js"></script>
        <script src="/js/jquery/jquery.ba-bbq.js"></script>

        <!-- Bootstrap JS -->
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        
        <!-- Backbone.js -->
        <script src="/js/backbone/underscore-min.js"></script>
        <script src="/js/backbone/json2.js"></script>
        <script src="/js/backbone/backbone.js"></script>

        <!-- Settings -->
        <script src="/js/app/Settings.js"></script>

        <!-- Models -->
        <script src="/js/app/models/Category.js"></script>
        <script src="/js/app/models/Eshop.js"></script>
        <script src="/js/app/models/Product.js"></script>

        <!-- Views -->
        <script src="/js/app/views/admin/Homepage.js"></script>
        <script src="/js/app/views/admin/CategoriesView.js"></script>
        <script src="/js/app/views/admin/CategoryList.js"></script>
        <script src="/js/app/views/admin/CategoryDetail.js"></script>
        <script src="/js/app/views/admin/EshopsView.js"></script>
        <script src="/js/app/views/admin/EshopTable.js"></script>
        <script src="/js/app/views/admin/EshopDetail.js"></script>
        <script src="/js/app/views/admin/EshopProductList.js"></script>
        <script src="/js/app/views/admin/ProductDetail.js"></script>
        <script src="/js/app/views/admin/ProductList.js"></script>
        <script src="/js/app/views/admin/ProductsView.js"></script>
        <script src="/js/app/views/admin/ProductLinkModal.js"></script>
        <script src="/js/app/views/Paginator.js"></script>

        <!-- Router - application starts -->
        <script src="/js/app/router/AdminRouter.js"></script>
    </body>
</html>

