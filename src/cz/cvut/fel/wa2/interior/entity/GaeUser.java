package cz.cvut.fel.wa2.interior.entity;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

/**
 *
 * @author Ondrej Panek
 */
@PersistenceCapable
public class GaeUser extends AbstractEntity {

    @Persistent
    private String email;

    public GaeUser(String email) {
        this.email = email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

}
