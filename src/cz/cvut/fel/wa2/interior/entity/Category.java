package cz.cvut.fel.wa2.interior.entity;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

/**
 *
 * @author Ondrej Panek
 */
@PersistenceCapable(detachable = "true")
public class Category extends AbstractEntity {

    @Persistent
    private String name;

    public Category() {
    }

    public Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String id = key != null ? key.getId() + "" : "";
        return "Category{" + "id=" + id + ", name=" + name + '}';
    }

}
