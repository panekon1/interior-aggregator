package cz.cvut.fel.wa2.interior.entity;

import java.util.ArrayList;
import java.util.List;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

/**
 *
 * @author Ondrej Panek
 */
@PersistenceCapable(detachable = "true")
public class Eshop extends AbstractEntity {

    @Persistent
    private String name;

    @Persistent
    private String web;

    @Persistent
    private String feed;

    @Persistent(mappedBy = "eshop")
    private List<Product> products = new ArrayList<>();

    public Eshop() {
    }

    public Eshop(String name, String web, String feed) {
        this.name = name;
        this.web = web;
        this.feed = feed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void addProduct(Product product) {
        products.add(product);
        product.setEshop(this);
    }

    public void removeProduct(Product product) {
        products.remove(product);
    }

    @Override
    public String toString() {
        String id = key != null ? key.getId() + "" : "";
        return "Eshop{" + "id=" + id + ", name=" + name + ", web=" + web + ", feed=" + feed + '}';
    }

}
