package cz.cvut.fel.wa2.interior.entity;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.datanucleus.annotations.Unowned;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

/**
 *
 * @author Ondrej Panek
 */
@PersistenceCapable
public class Product extends AbstractEntity {

    @Persistent
    private String name;

    @Persistent
    private String description;

    @Persistent(defaultFetchGroup = "true")
    @Unowned
    private Category category;

    @Persistent(defaultFetchGroup = "true")
    private Eshop eshop;

    @Persistent
    private String url;

    @Persistent
    private float price;

    @Persistent
    private float priceWithVAT;

    @Persistent
    private String imageURL;

    @Persistent
    private Set<Key> similarProductKeys = new HashSet<>();

    @NotPersistent
    private Set<Product> similiarProducts = new HashSet<>();

    public Product() {
    }

    public Product(String name, String description, Category category, Eshop eshop, String url, float price, float priceWithVAT, String imageURL) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.eshop = eshop;
        this.url = url;
        this.price = price;
        this.priceWithVAT = priceWithVAT;
        this.imageURL = imageURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Eshop getEshop() {
        return eshop;
    }

    public void setEshop(Eshop eshop) {
        this.eshop = eshop;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPriceWithVAT() {
        return priceWithVAT;
    }

    public void setPriceWithVAT(float priceWithVAT) {
        this.priceWithVAT = priceWithVAT;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Set<Key> getSimilarProductKeys() {
        return similarProductKeys;
    }

    public void addSimilarProduct(Product product) {
        similarProductKeys.add(product.getKey());
    }

    public void removeSimilarProduct(Product product) {
        similarProductKeys.remove(product.getKey());
    }

    public void setSimilarProducts(Set<Product> similiarProducts) {
        this.similiarProducts = similiarProducts;
    }

    public Set<Product> getSimilarProducts() {
        return similiarProducts;
    }

    @Override
    public String toString() {
        String id = key != null ? key.getId() + "" : "";
        return "Product{" + "id=" + id + ", name=" + name + ", description=" + description + ", category=" + category + ", eshop=" + eshop + ", url=" + url + ", price=" + price + ", priceWithVAT=" + priceWithVAT + ", imageURL=" + imageURL + ", similarProducts=" + similarProductKeys + '}';
    }

}
