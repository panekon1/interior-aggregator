package cz.cvut.fel.wa2.interior.dto;

/**
 *
 * @author Ondrej Panek
 */
public abstract class AbstractDTO {

    protected Long id;
    protected String uri;

    public AbstractDTO() {
    }

    public AbstractDTO(Long id) {
	this.id = id;
    }

    public AbstractDTO(Long id, String uri) {
	this.uri = uri;
	this.id = id;
    }

    public String getUri() {
	return uri;
    }

    public Long getId() {
	return id;
    }

}
