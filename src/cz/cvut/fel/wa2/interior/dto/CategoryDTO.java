package cz.cvut.fel.wa2.interior.dto;

import cz.cvut.fel.wa2.interior.entity.Category;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ondrej Panek
 */
@XmlRootElement(name = "category")
public class CategoryDTO extends AbstractDTO {

    private String name;

    public CategoryDTO() {
    }

    public CategoryDTO(Category category, UriInfo uriInfo) {
        super(category.getKey().getId(), uriInfo.getBaseUri() + "category/" + category.getKey().getId());
        this.name = category.getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
