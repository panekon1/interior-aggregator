package cz.cvut.fel.wa2.interior.dto;

import cz.cvut.fel.wa2.interior.entity.Eshop;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ondrej Panek
 */
@XmlRootElement(name = "eshop")
public class EshopDTO extends AbstractDTO {

    private String name;
    private String web;
    private String feed;

    public EshopDTO() {
    }

    public EshopDTO(Eshop eshop, UriInfo uriInfo) {
        super(eshop.getKey().getId(), uriInfo.getBaseUri() + "eshop/" + eshop.getKey().getId());
        this.name = eshop.getName();
        this.web = eshop.getWeb();
        this.feed = eshop.getFeed();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getFeed() {
        return feed;
    }

    public void setFeed(String feed) {
        this.feed = feed;
    }

}
