package cz.cvut.fel.wa2.interior.dto;

import cz.cvut.fel.wa2.interior.entity.Product;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ondrej Panek
 */
@XmlRootElement(name = "product")
public class ProductDTO extends AbstractDTO {

    private String name;
    private String description;
    private CategoryDTO category;
    private EshopDTO eshop;
    private String url;
    private float price;
    private float priceWithVAT;
    private String imageURL;
    private Set<ProductDTO> similarProducts = new HashSet<>();

    public ProductDTO() {
    }

    public ProductDTO(Product product, UriInfo uriInfo) {
        this(product, uriInfo, false);
    }

    public ProductDTO(Product product, UriInfo uriInfo, boolean eager) {
        super(product.getKey().getId(), uriInfo.getBaseUri() + "eshop/" + product.getEshop().getKey().getId() + "/product/" + product.getKey().getId());
        this.name = product.getName();
        this.description = product.getDescription();
        if (product.getCategory() != null) {
            this.category = new CategoryDTO(product.getCategory(), uriInfo);
        }
        if (product.getEshop() != null) {
            this.eshop = new EshopDTO(product.getEshop(), uriInfo);
        }
        this.url = product.getUrl();
        this.price = product.getPrice();
        this.priceWithVAT = product.getPriceWithVAT();
        this.imageURL = product.getImageURL();
        if (eager) {
            for (Product similiarProduct : product.getSimilarProducts()) {
                similarProducts.add(new ProductDTO(similiarProduct, uriInfo, false));
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CategoryDTO getCategory() {
        return category;
    }

    public void setCategory(CategoryDTO category) {
        this.category = category;
    }

    public EshopDTO getEShop() {
        return eshop;
    }

    public void setEShop(EshopDTO eshop) {
        this.eshop = eshop;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPriceWithVAT() {
        return priceWithVAT;
    }

    public void setPriceWithVAT(float priceWithVAT) {
        this.priceWithVAT = priceWithVAT;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Set<ProductDTO> getSimilarProducts() {
        return similarProducts;
    }

    public void setSimilarProducts(Set<ProductDTO> similarProducts) {
        this.similarProducts = similarProducts;
    }

    public void addSimilarProducts(ProductDTO product) {
        similarProducts.add(product);
    }

    public void removeSimilarProducts(ProductDTO product) {
        similarProducts.remove(product);
    }

}
