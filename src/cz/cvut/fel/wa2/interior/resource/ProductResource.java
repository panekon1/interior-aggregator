package cz.cvut.fel.wa2.interior.resource;

import com.sun.jersey.api.NotFoundException;
import cz.cvut.fel.wa2.interior.dto.ProductDTO;
import cz.cvut.fel.wa2.interior.service.NotExistingEntityException;
import cz.cvut.fel.wa2.interior.service.ProductService;
import cz.cvut.fel.wa2.interior.service.Result;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ondrej Panek
 */
@Component
@Scope("request")
@Path("/product/")
public class ProductResource {

    @Autowired
    private ProductService productService;

    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findBy(@Context UriInfo uriInfo, @HeaderParam("X-Search") String searchString, @HeaderParam("X-Category") Long categoryId,
            @HeaderParam("X-Price-From") String priceFromString, @HeaderParam("X-Price-To") String priceToString, @HeaderParam("X-Order") String order,
            @HeaderParam("X-Base") String baseString, @HeaderParam("X-Offset") String offsetString) {

        Integer base = null;
        Integer offset = null;
        if (baseString != null && offsetString != null) {
            try {
                base = Integer.parseInt(baseString);
                offset = Integer.parseInt(offsetString);
            } catch (NumberFormatException e) {
                throw new BadRequestException("Invalid pagination header attributes.");
            }
        }

        Float priceFrom = null;
        if (priceFromString != null) {
            try {
                priceFrom = Float.parseFloat(priceFromString);
            } catch (NumberFormatException e) {
                throw new BadRequestException("Invalid X-Price-From header attribute.");
            }
        }

        Float priceTo = null;
        if (priceToString != null) {
            try {
                priceTo = Float.parseFloat(priceToString);
            } catch (NumberFormatException e) {
                throw new BadRequestException("Invalid X-Price-To header attribute.");
            }
        }

        try {
            Result<ProductDTO> result = productService.findBy(uriInfo, searchString, categoryId, priceFrom, priceTo, order, base, offset);
            return Response.status(Response.Status.OK).header("X-Count", result.getCount()).entity(result.getData()).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }
    }

}
