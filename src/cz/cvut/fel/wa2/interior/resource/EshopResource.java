package cz.cvut.fel.wa2.interior.resource;

import com.sun.jersey.api.NotFoundException;
import cz.cvut.fel.wa2.interior.dto.EshopDTO;
import cz.cvut.fel.wa2.interior.dto.ProductDTO;
import cz.cvut.fel.wa2.interior.service.EshopService;
import cz.cvut.fel.wa2.interior.service.NotExistingEntityException;
import cz.cvut.fel.wa2.interior.service.ProductService;
import cz.cvut.fel.wa2.interior.service.Result;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ondrej Panek
 */
@Component
@Scope("request")
@Path("/eshop/")
public class EshopResource {

    @Autowired
    private EshopService eshopService;

    @Autowired
    private ProductService productService;

    @POST
    @Path("/")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createEshop(@Context UriInfo uriInfo, EshopDTO eshopDTO) {
        EshopDTO eshop = eshopService.create(uriInfo, eshopDTO);
        return Response.status(Response.Status.CREATED)
                .header("Location", eshop.getUri()).build();
    }

    @PUT
    @Path("/{id:\\d+}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateEshop(@PathParam("id") Long id, EshopDTO eshopDTO) {
        try {
            eshopService.update(id, eshopDTO);
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }
    }

    @DELETE
    @Path("/{id:\\d+}")
    public Response deleteEshop(@PathParam("id") Long id) {
        eshopService.delete(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @GET
    @Path("/{id:\\d+}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findEshop(@Context UriInfo uriInfo, @PathParam("id") Long id) {
        try {
            EshopDTO eshop = eshopService.findById(uriInfo, id);
            return Response.status(Response.Status.OK).entity(eshop).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }
    }

    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findAllEshops(@Context UriInfo uriInfo) {
        List<EshopDTO> eshops = eshopService.findAll(uriInfo);
        return Response.status(Response.Status.OK).entity(eshops).build();
    }

    @POST
    @Path("/{eshopId:\\d+}/product/")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createProduct(@Context UriInfo uriInfo, @PathParam("eshopId") Long eshopId, ProductDTO productDTO) {
        try {
            ProductDTO product = productService.create(uriInfo, eshopId, productDTO);
            return Response.status(Response.Status.CREATED)
                    .header("Location", product.getUri()).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }
    }

    @PUT
    @Path("/{eshopId:\\d+}/product/{productId:\\d+}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateProduct(@PathParam("eshopId") Long eshopId, @PathParam("productId") Long productId, ProductDTO productDTO) {
        try {
            productService.update(eshopId, productId, productDTO);
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }
    }

    @DELETE
    @Path("/{eshopId:\\d+}/product/{productId:\\d+}")
    public Response deleteProduct(@PathParam("eshopId") Long eshopId, @PathParam("productId") Long productId) {
        productService.delete(eshopId, productId);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @GET
    @Path("/{eshopId:\\d+}/product/{productId:\\d+}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findProduct(@Context UriInfo uriInfo, @PathParam("eshopId") Long eshopId, @PathParam("productId") Long productId) {
        try {
            ProductDTO product = productService.findById(uriInfo, eshopId, productId);
            return Response.status(Response.Status.OK).entity(product).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }
    }

    @GET
    @Path("/{eshopId:\\d+}/product")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findAllProductsFromEShop(@Context UriInfo uriInfo, @PathParam("eshopId") Long eshopId, @HeaderParam("X-Order") String order,
            @HeaderParam("X-Base") String baseString, @HeaderParam("X-Offset") String offsetString) {

        Integer base = null;
        Integer offset = null;
        if (baseString != null && offsetString != null) {
            try {
                base = Integer.parseInt(baseString);
                offset = Integer.parseInt(offsetString);
            } catch (NumberFormatException e) {
                throw new BadRequestException("Invalid pagination header attributes.");
            }
        }

        try {
            Result<ProductDTO> result = productService.findByEshop(uriInfo, eshopId, order, base, offset);
            return Response.status(Response.Status.OK).header("X-Count", result.getCount()).entity(result.getData()).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }

    }

    @POST
    @Path("/{eshopId:\\d+}/product/{productId:\\d+}/link/{linkedEshopId:\\d+}/{linkedProductId:\\d+}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response linkProducts(@PathParam("eshopId") Long eshopId, @PathParam("productId") Long productId,
            @PathParam("linkedEshopId") Long linkedEshopId, @PathParam("linkedProductId") Long linkedProductId) {
        try {
            productService.linkProduct(eshopId, productId, linkedEshopId, linkedProductId);
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }
    }
    
    @POST
    @Path("/{eshopId:\\d+}/product/{productId:\\d+}/unlink/{linkedEshopId:\\d+}/{linkedProductId:\\d+}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response unlinkProducts(@PathParam("eshopId") Long eshopId, @PathParam("productId") Long productId,
            @PathParam("linkedEshopId") Long linkedEshopId, @PathParam("linkedProductId") Long linkedProductId) {
        try {
            productService.unlinkProduct(eshopId, productId, linkedEshopId, linkedProductId);
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }
    }

}
