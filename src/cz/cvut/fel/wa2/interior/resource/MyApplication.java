package cz.cvut.fel.wa2.interior.resource;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Ondrej Panek
 */
public class MyApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<>();
        s.add(RootResource.class);
        s.add(EshopResource.class);
        s.add(CategoryResource.class);
        s.add(ProductResource.class);
        return s;
    }

}
