package cz.cvut.fel.wa2.interior.resource;

import cz.cvut.fel.wa2.interior.service.UserService;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ondrej Panek
 */
@Component
@Scope("request")
@Path("/")
public class RootResource {

    @Autowired
    private UserService userService;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/")
    public Response singleEntryPoint(@Context UriInfo uriInfo) {

        // Add administrators
        if (userService.findAll().isEmpty()) {
            userService.create("panekon1@gmail.com");
            userService.create("dan.p.ja@gmail.com");
        }

        Map<String, String> root = new HashMap<>();
        root.put("category", uriInfo.getBaseUri() + "category");
        root.put("eshop", uriInfo.getBaseUri() + "eshop");
        root.put("product", uriInfo.getBaseUri() + "product");

        return Response.status(Response.Status.OK).entity(root).build();
    }
}
