package cz.cvut.fel.wa2.interior.resource;

import com.sun.jersey.api.NotFoundException;
import cz.cvut.fel.wa2.interior.dto.CategoryDTO;
import cz.cvut.fel.wa2.interior.dto.ProductDTO;
import cz.cvut.fel.wa2.interior.service.CategoryService;
import cz.cvut.fel.wa2.interior.service.NotExistingEntityException;
import cz.cvut.fel.wa2.interior.service.ProductService;
import cz.cvut.fel.wa2.interior.service.Result;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ondrej Panek
 */
@Component
@Scope("request")
@Path("/category/")
public class CategoryResource {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;

    @POST
    @Path("/")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createCategory(@Context UriInfo uriInfo, CategoryDTO categoryDTO) {
        CategoryDTO category = categoryService.create(uriInfo, categoryDTO);
        return Response.status(Response.Status.CREATED)
                .header("Location", category.getUri()).build();
    }

    @PUT
    @Path("/{id:\\d+}")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateCategory(@PathParam("id") Long id, CategoryDTO categoryDTO) {
        try {
            categoryService.update(id, categoryDTO);
            return Response.status(Response.Status.NO_CONTENT).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }
    }

    @DELETE
    @Path("/{id:\\d+}")
    public Response deleteCategory(@PathParam("id") Long id) {
        categoryService.delete(id);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @GET
    @Path("/{id:\\d+}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findCategory(@Context UriInfo uriInfo, @PathParam("id") Long id) {
        try {
            CategoryDTO category = categoryService.findById(uriInfo, id);
            return Response.status(Response.Status.OK).entity(category).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }
    }

    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findAllCategories(@Context UriInfo uriInfo) {
        List<CategoryDTO> categories = categoryService.findAll(uriInfo);
        return Response.status(Response.Status.OK).entity(categories).build();
    }

    @GET
    @Path("/{id:\\d+}/product")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findAllProductOfCategory(@Context UriInfo uriInfo, @PathParam("id") Long id, @HeaderParam("X-Order") String order,
            @HeaderParam("X-Base") String baseString, @HeaderParam("X-Offset") String offsetString) {
        Integer base = null;
        Integer offset = null;
        if (baseString != null && offsetString != null) {
            try {
                base = Integer.parseInt(baseString);
                offset = Integer.parseInt(offsetString);
            } catch (NumberFormatException e) {
                throw new BadRequestException("Invalid pagination header attributes.");
            }
        }

        try {
            Result<ProductDTO> result = productService.findByCategory(uriInfo, id, order, base, offset);
            return Response.status(Response.Status.OK).header("X-Count", result.getCount()).entity(result.getData()).build();
        } catch (NotExistingEntityException ex) {
            throw new NotFoundException(ex.getMessage());
        }

    }
}
