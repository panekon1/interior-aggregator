package cz.cvut.fel.wa2.interior.authentication;

import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author Ondrej Panek
 */
public interface TokenUtils {

    String getToken(UserDetails userDetails);

    String getToken(UserDetails userDetails, Long expiration);

    boolean validate(String token);

    UserDetails getUserFromToken(String token);
}
