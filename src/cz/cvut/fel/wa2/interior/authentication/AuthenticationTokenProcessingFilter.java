package cz.cvut.fel.wa2.interior.authentication;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;
import cz.cvut.fel.wa2.interior.dao.UserDAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.filter.GenericFilterBean;

/**
 *
 * @author Ondrej Panek
 */
public class AuthenticationTokenProcessingFilter extends GenericFilterBean {

    @Autowired
    private TokenUtils tokenUtils;
    private final AuthenticationManager authManager;
    private final AuthenticationDetailsSource ads = new WebAuthenticationDetailsSource();
    private final static String AUTHENTICATION_HEADER = "X-Authentication-Token";

    public AuthenticationTokenProcessingFilter(AuthenticationManager authManager) {
        this.authManager = authManager;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String token = httpRequest.getHeader(AUTHENTICATION_HEADER);

        if (token != null && !token.equals("null")) {
         //   System.out.println("RECEIVED TOKEN: " + token);
            // validate the token
            if (tokenUtils.validate(token)) {
                // determine the user based on the (already validated) token
                UserDetails user = tokenUtils.getUserFromToken(token);

                // build an Authentication object with the user's info
                PreAuthenticatedAuthenticationToken authentication = new PreAuthenticatedAuthenticationToken(user, null);
                authentication.setDetails(ads.buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authManager.authenticate(authentication));

                // Add token to HTTP header
                if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
                    // Add token to HTTP header
                    HttpServletResponse httpResponse = (HttpServletResponse) response;
                    httpResponse.addHeader("X-Authentication-Token", token);
                }
            }
        } else {
        //    System.out.println("DONT HAVE TOKEN");
            // If there is not any token, check if there is a Google Accounts user
            User googleUser = UserServiceFactory.getUserService().getCurrentUser();
            // System.out.println(googleUser);
            if (googleUser != null) {
                UserDetails user = new org.springframework.security.core.userdetails.User(googleUser.getEmail(), "", new ArrayList<GrantedAuthority>());

                // build an Authentication object with the user's info
                PreAuthenticatedAuthenticationToken authentication = new PreAuthenticatedAuthenticationToken(user, null);
                authentication.setDetails(ads.buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authManager.authenticate(authentication));

                if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
                    // Add token to HTTP header
                    token = tokenUtils.getToken(user);
                    HttpServletResponse httpResponse = (HttpServletResponse) response;
                    httpResponse.addHeader("X-Authentication-Token", token);
                }
            }
        }
        // continue through the filter chain
        chain.doFilter(request, response);
    }
}
