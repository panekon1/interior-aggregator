package cz.cvut.fel.wa2.interior.authentication;

import cz.cvut.fel.wa2.interior.entity.GaeUser;
import cz.cvut.fel.wa2.interior.service.UserService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.UUID;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class TokenUtilsImpl implements TokenUtils {

    @Autowired
    private UserService userService;
    private final StandardPBEStringEncryptor jasypt;
    private static final String DELIMITER = "|";
    private static final String PASSWORD = "t2?s#c42D";

    public TokenUtilsImpl() {
        jasypt = new StandardPBEStringEncryptor();
        jasypt.setPassword(PASSWORD);
    }

    @Override
    public String getToken(UserDetails userDetails) {
        String key = UUID.randomUUID().toString().toUpperCase()
                + DELIMITER + userDetails.getUsername()
                + DELIMITER + new Date().toString();
        return jasypt.encrypt(key);
    }

    @Override
    public String getToken(UserDetails userDetails, Long expiration) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean validate(String token) {
        String decryptedToken = jasypt.decrypt(token);
        StringTokenizer tokenizer = new StringTokenizer(decryptedToken);

        tokenizer.nextToken(DELIMITER);
        String email = tokenizer.nextToken(DELIMITER);

        return userService.findByEmail(email) != null;
    }

    @Override
    public UserDetails getUserFromToken(String token) {
        String decryptedToken = jasypt.decrypt(token);
        StringTokenizer tokenizer = new StringTokenizer(decryptedToken);

        tokenizer.nextToken(DELIMITER);
        String email = tokenizer.nextToken(DELIMITER);

        Collection<GrantedAuthority> authorities = new ArrayList<>();

        return new User(email, "", authorities);
    }

}
