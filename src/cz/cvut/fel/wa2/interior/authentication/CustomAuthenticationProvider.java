package cz.cvut.fel.wa2.interior.authentication;


import cz.cvut.fel.wa2.interior.entity.GaeUser;
import cz.cvut.fel.wa2.interior.service.UserService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

/**
 *
 * @author Ondrej Panek
 */
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        //  System.out.println("AUTHENTICATE");
        User user = (User) authentication.getPrincipal();
       // System.out.println("AUTHENTICATING " + user.getUsername());

        CustomAuthentication gaeAuthentication = new CustomAuthentication(user, authentication.getDetails());

        GaeUser googleUser = userService.findByEmail(user.getUsername());
        if (googleUser == null) {
        //    System.out.println("NOT AUTHENTICATED " + user.getUsername());
            gaeAuthentication.setAuthenticated(false);
        }

        return gaeAuthentication;
    }

    @Override
    public final boolean supports(Class<?> authentication) {
        return PreAuthenticatedAuthenticationToken.class.isAssignableFrom(authentication);
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

}
