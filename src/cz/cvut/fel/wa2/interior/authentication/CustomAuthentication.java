package cz.cvut.fel.wa2.interior.authentication;

import java.util.ArrayList;
import java.util.Collection;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 *
 * @author Ondrej Panek
 */
public class CustomAuthentication implements Authentication {

    private final User user;
    private final Object details;
    private boolean authenticated;

    public CustomAuthentication(User user, Object details) {
        this.user = user;
        this.details = details;
        authenticated = true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList<>();
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return details;
    }

    @Override
    public Object getPrincipal() {
        return user;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean bln) throws IllegalArgumentException {
        authenticated = bln;
    }

    @Override
    public String getName() {
        return user.getUsername();
    }

}
