package cz.cvut.fel.wa2.interior.dao;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import cz.cvut.fel.wa2.interior.entity.AbstractEntity;
import cz.cvut.fel.wa2.interior.entity.Product;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jdo.TransactionAwarePersistenceManagerFactoryProxy;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ondrej Panek
 */
@Repository
public abstract class GenericDAO {

    @Autowired
    protected TransactionAwarePersistenceManagerFactoryProxy persistenceManagerFactoryProxy;

    protected PersistenceManager createPersistenceManager() {
        return persistenceManagerFactoryProxy.getTargetPersistenceManagerFactory().getPersistenceManager();
    }

    public <ENTITY extends AbstractEntity> ENTITY persist(ENTITY o) {
        PersistenceManager pm = createPersistenceManager();
        try {
            pm.makePersistent(o);
            pm.flush();
        } finally {
            pm.close();
        }
        return o;
    }

    public <ENTITY extends AbstractEntity> void persistAll(Collection<ENTITY> entities) {
        PersistenceManager pm = createPersistenceManager();
        try {
            pm.makePersistentAll(entities);
            pm.flush();
        } finally {
            pm.close();
        }
    }

    public <ENTITY extends AbstractEntity> void update(ENTITY o) {
        PersistenceManager pm = createPersistenceManager();
        try {
            pm.makePersistent(o);
            pm.flush();
        } finally {
            pm.close();
        }
    }

    public <ENTITY extends AbstractEntity> void updateAll(Collection<ENTITY> entities) {
        PersistenceManager pm = createPersistenceManager();
        try {
            pm.makePersistentAll(entities);
            pm.flush();
        } finally {
            pm.close();
        }
    }

    public <ENTITY extends AbstractEntity> void removeById(Class<ENTITY> clazz, Long id) {
        PersistenceManager pm = createPersistenceManager();
        Key key = KeyFactory.createKey(clazz.getSimpleName(), id);
        try {
            ENTITY object = (ENTITY) pm.getObjectById(clazz, key);
            if (object != null) {
                pm.deletePersistent(object);
                pm.flush();
            }
        } catch (JDOObjectNotFoundException e) {
            return;
        } finally {
            pm.close();
        }
    }

    public <ENTITY extends AbstractEntity> void removeByComplexId(Class<ENTITY> clazz, Long id, Class parentClazz, Long parentId) {
        PersistenceManager pm = createPersistenceManager();
        Key key = KeyFactory.createKey(KeyFactory.createKey(parentClazz.getSimpleName(), parentId), clazz.getSimpleName(), id);
        try {
            ENTITY object = (ENTITY) pm.getObjectById(clazz, key);
            if (object != null) {
                pm.deletePersistent(object);
                pm.flush();
            }
        } catch (JDOObjectNotFoundException e) {
            return;
        } finally {
            pm.close();
        }
    }

    public <ENTITY extends AbstractEntity> ENTITY findById(Class<ENTITY> clazz, Long id) {
        PersistenceManager pm = createPersistenceManager();
        Key key = KeyFactory.createKey(clazz.getSimpleName(), id);

        ENTITY object = null;
        try {
            object = (ENTITY) pm.getObjectById(clazz, key);
        } catch (JDOObjectNotFoundException e) {
            return null;
        } finally {
            pm.close();
        }
        return object;
    }
    
    public <ENTITY extends AbstractEntity> ENTITY findById(Class<ENTITY> clazz, String id) {
        PersistenceManager pm = createPersistenceManager();
        Key key = KeyFactory.createKey(clazz.getSimpleName(), id);

        ENTITY object = null;
        try {
            object = (ENTITY) pm.getObjectById(clazz, key);
        } catch (JDOObjectNotFoundException e) {
            return null;
        } finally {
            pm.close();
        }
        return object;
    }

    public <ENTITY extends AbstractEntity> ENTITY findByComplexId(Class<ENTITY> clazz, Long id, Class parentClazz, Long parentId) {
        PersistenceManager pm = createPersistenceManager();
        Key key = KeyFactory.createKey(KeyFactory.createKey(parentClazz.getSimpleName(), parentId), clazz.getSimpleName(), id);
        ENTITY entity = null;
        try {
            entity = (ENTITY) pm.getObjectById(clazz, key);
        } catch (JDOObjectNotFoundException e) {
            return null;
        } finally {
            pm.close();
        }
        return entity;
    }

    public <ENTITY extends AbstractEntity> List<ENTITY> findAll(Class<ENTITY> clazz, String orderProperty, Boolean asc) {
        PersistenceManager pm = createPersistenceManager();

        List<ENTITY> results = new ArrayList<>();
        try {
            Query query = pm.newQuery(clazz);
            String direction = asc ? "asc" : "desc";
            query.setOrdering(orderProperty + " " + direction);

            results = (List<ENTITY>) query.execute();
        } finally {
            pm.close();
        }
        return results;
    }

    public <ENTITY extends AbstractEntity> long findAllCount(Class<ENTITY> clazz) {
        PersistenceManager pm = createPersistenceManager();

        long count = 0;
        try {
            Query query = pm.newQuery(clazz);
            query.setResult("count(id)");

            count = (long) query.execute();
        } finally {
            pm.close();
        }
        return count;
    }
    
    public <ENTITY extends AbstractEntity> List<ENTITY> findByProperty(Class<ENTITY> clazz, String property, Class valueClass, Object value) {
        PersistenceManager pm = createPersistenceManager();
        
        List<ENTITY> results = new ArrayList<>();
        try {
            Query query = pm.newQuery(clazz);
            query.setFilter(property + " == value");
            query.declareParameters(valueClass.getName() + " value");
            
            results = (List<ENTITY>) query.execute(value);
        } finally {
            pm.close();
        }
        return results;
    }

    public long findByPropertyCount(Class clazz, String property, Class valueClass, Object value) {
        PersistenceManager pm = createPersistenceManager();

        long count = 0;
        try {
            Query query = pm.newQuery(clazz);
            query.setFilter(property + " == value");
            query.declareParameters(valueClass.getName() + " value");
            query.setResult("count(id)");

            count = (long) query.execute(value);

        } finally {
            pm.close();
        }
        return count;
    }
}
