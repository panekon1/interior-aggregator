package cz.cvut.fel.wa2.interior.dao;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import cz.cvut.fel.wa2.interior.entity.Eshop;
import cz.cvut.fel.wa2.interior.entity.Product;
import java.util.HashSet;
import java.util.Set;
import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ondrej Panek
 */
@Repository
public class EshopDAO extends GenericDAO {

    public Eshop findEagerlyById(Class clazz, Long id) {
        PersistenceManager pm = createPersistenceManager();
        Key key = KeyFactory.createKey(clazz.getSimpleName(), id);

        Eshop eshop = null;
        try {
            eshop = (Eshop) pm.getObjectById(clazz, key);
            for (Product product : eshop.getProducts()) {
                product.getName();
                product.getCategory();
                Set<Product> similarProducts = new HashSet<>();
                for (Key similarProductKey : product.getSimilarProductKeys()) {
                    similarProducts.add((Product) pm.getObjectById(Product.class, similarProductKey));
                }
                product.setSimilarProducts(similarProducts);
            }
        } catch (JDOObjectNotFoundException e) {
            return null;
        } finally {
            pm.close();
        }
        return eshop;
    }

}
