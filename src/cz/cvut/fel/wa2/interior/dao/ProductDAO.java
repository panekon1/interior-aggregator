package cz.cvut.fel.wa2.interior.dao;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import cz.cvut.fel.wa2.interior.entity.Category;
import cz.cvut.fel.wa2.interior.entity.Product;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDAO extends GenericDAO {

    @Override
    public Product findByComplexId(Class clazz, Long productId, Class parentClazz, Long eshopId) {
        PersistenceManager pm = createPersistenceManager();
        Key key = KeyFactory.createKey(KeyFactory.createKey(parentClazz.getSimpleName(), eshopId), clazz.getSimpleName(), productId);
        Product product = null;
        try {
            product = (Product) pm.getObjectById(clazz, key);

            // must touch lazily-loaded fields because joins aren't supported
            product.getEshop().getName();
            product.getCategory();

            Set<Product> similarProducts = new HashSet<>();
            for (Key similarProductKey : product.getSimilarProductKeys()) {
                similarProducts.add((Product) pm.getObjectById(clazz, similarProductKey));
            }
            product.setSimilarProducts(similarProducts);
        } catch (JDOObjectNotFoundException e) {
            return null;
        } finally {
            pm.close();
        }
        return product;
    }

    @Override
    public List<Product> findAll(Class clazz, String orderProperty, Boolean asc) {
        return findAll(clazz, orderProperty, asc, null, null);
    }

    public List<Product> findAll(Class clazz, String orderProperty, Boolean asc, Integer base, Integer offset) {
        PersistenceManager pm = createPersistenceManager();

        List<Product> results = new ArrayList<>();
        try {
            Query query = pm.newQuery(clazz);
            String direction = asc ? "asc" : "desc";
            query.setOrdering(orderProperty + " " + direction);
            if (base != null && offset != null) {
                query.setRange(base, base + offset);
            }

            results = (List<Product>) query.execute();

            // must touch lazily-loaded fields because joins aren't supported
            for (Product product : results) {
                product.getEshop();
                product.getCategory();
                product.getSimilarProductKeys();
            }
        } finally {
            pm.close();
        }
        return results;
    }

    public List<Product> findByProperty(Class clazz, String property, Class valueClass, Object value, String orderProperty, Boolean asc, Integer base, Integer offset) {
        PersistenceManager pm = createPersistenceManager();

        List<Product> results = new ArrayList<>();
        try {
            Query query = pm.newQuery(clazz);
            query.setFilter(property + " == value");
            query.declareParameters(valueClass.getName() + " value");

            String direction = asc ? "asc" : "desc";
            query.setOrdering(orderProperty + " " + direction);
            if (base != null && offset != null) {
                query.setRange(base, base + offset);
            }

            results = (List<Product>) query.execute(value);
        } finally {
            pm.close();
        }
        return results;
    }

    public List<Product> findBy(Class clazz, String searchString, Category category, Float priceFrom, Float priceTo, String orderProperty, Boolean asc, Integer base, Integer offset) {
        PersistenceManager pm = createPersistenceManager();

        Map<String, Object> paramMap = new HashMap<>();
        String filterString = "", declareString = "";

        if (searchString != null) {
            searchString = Character.toUpperCase(searchString.charAt(0)) + searchString.substring(1);
            filterString += "name >= searchStringFrom && name <= searchStringTo";
            declareString += "String searchStringFrom, String searchStringTo";
            paramMap.put("searchStringFrom", searchString);
            paramMap.put("searchStringTo", searchString + "\ufffd");
        } else {
            if (category != null) {
                if (filterString.length() != 0) {
                    filterString += " && ";
                }
                filterString += "category == categoryKey";
                if (declareString.length() != 0) {
                    declareString += ",";
                }
                declareString += "com.google.appengine.api.datastore.Key categoryKey";
                paramMap.put("categoryKey", category.getKey());
            }

            if (priceFrom != null) {
                if (filterString.length() != 0) {
                    filterString += " && ";
                }
                filterString += "priceWithVAT >= priceFrom";
                if (declareString.length() != 0) {
                    declareString += ",";
                }
                declareString += "Float priceFrom";
                paramMap.put("priceFrom", priceFrom);
            }

            if (priceTo != null) {
                if (filterString.length() != 0) {
                    filterString += " && ";
                }
                filterString += "priceWithVAT <= priceTo";
                if (declareString.length() != 0) {
                    declareString += ",";
                }
                declareString += "Float priceTo";
                paramMap.put("priceTo", priceTo);
            }
        }

        List<Product> results = new ArrayList<>();
        try {
            Query query = pm.newQuery(clazz);

            query.setFilter(filterString);
            query.declareParameters(declareString);

            String direction = asc ? "asc" : "desc";
            //query.setOrdering(orderProperty + " " + direction);
            if (base != null && offset != null) {
                query.setRange(base, base + offset);
            }

            results = (List<Product>) query.executeWithMap(paramMap);
        } finally {
            pm.close();
        }
        return results;
    }

    public long findByCount(Class clazz, String searchString, Category category, Float priceFrom, Float priceTo) {
        PersistenceManager pm = createPersistenceManager();

        Map<String, Object> paramMap = new HashMap<>();
        String filterString = "", declareString = "";

        if (searchString != null) {
            searchString = Character.toUpperCase(searchString.charAt(0)) + searchString.substring(1);
            filterString += "name >= searchStringFrom && name <= searchStringTo";
            declareString += "String searchStringFrom, String searchStringTo";
            paramMap.put("searchStringFrom", searchString);
            paramMap.put("searchStringTo", searchString + "\ufffd");
        } else {
            if (category != null) {
                if (filterString.length() != 0) {
                    filterString += " && ";
                }
                filterString += "category == categoryKey";
                if (declareString.length() != 0) {
                    declareString += ",";
                }
                declareString += "com.google.appengine.api.datastore.Key categoryKey";
                paramMap.put("categoryKey", category.getKey());
            }

            if (priceFrom != null) {
                if (filterString.length() != 0) {
                    filterString += " && ";
                }
                filterString += "priceWithVAT >= priceFrom";
                if (declareString.length() != 0) {
                    declareString += ",";
                }
                declareString += "Float priceFrom";
                paramMap.put("priceFrom", priceFrom);
            }

            if (priceTo != null) {
                if (filterString.length() != 0) {
                    filterString += " && ";
                }
                filterString += "priceWithVAT <= priceTo";
                if (declareString.length() != 0) {
                    declareString += ",";
                }
                declareString += "Float priceTo";
                paramMap.put("priceTo", priceTo);
            }
        }

        long count = 0;
        try {
            Query query = pm.newQuery(clazz);

            query.setFilter(filterString);
            query.declareParameters(declareString);
            query.setResult("count(id)");

            count = (long) query.executeWithMap(paramMap);
        } finally {
            pm.close();
        }
        return count;
    }

}
