package cz.cvut.fel.wa2.interior.controller;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Ondrej Panek
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // If already logged in, go to admin page
        if (SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            //  System.out.println("AUTHENTICATED as " + SecurityContextHolder.getContext().getAuthentication().getName());
            response.sendRedirect("/admin/");
        } else {
            UserService userService = UserServiceFactory.getUserService();
            response.sendRedirect(userService.createLoginURL("/admin/"));
        }
    }

}
