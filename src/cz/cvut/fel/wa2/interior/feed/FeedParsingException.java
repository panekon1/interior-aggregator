package cz.cvut.fel.wa2.interior.feed;

/**
 *
 * @author Ondrej Panek
 */
public class FeedParsingException extends Exception {

    public FeedParsingException() {
    }

    public FeedParsingException(String message) {
        super(message);
    }

}
