package cz.cvut.fel.wa2.interior.feed;

import cz.cvut.fel.wa2.interior.entity.Eshop;
import cz.cvut.fel.wa2.interior.entity.Product;
import java.io.IOException;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Ondrej Panek
 */
@Component
public class FeedReader {

    private static final String SHOP_ITEM = "SHOPITEM";
    private static final String PRODUCT = "PRODUCT";
    private static final String URL = "URL";
    private static final String PRICE = "PRICE";
    private static final String PRICE_VAT = "PRICE_VAT";
    private static final String IMAGE_URL = "IMGURL";

    public void parseFeed(Eshop eshop) throws FeedNotFoundException, FeedParsingException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new URL(eshop.getFeed()).openStream());

            NodeList nodes = doc.getElementsByTagName(SHOP_ITEM);
            for (int i = 0; i < nodes.getLength(); i++) {
                Element node = (Element) nodes.item(i);

                String name = "";
                String description = "";
                String url = "";
                Float price = 0f;
                Float priceWithVAT = 0f;
                String imageURL = "";

                NodeList products = node.getElementsByTagName(PRODUCT);
                if (products.getLength() > 0) {
                    Element productEl = (Element) products.item(0);
                    name = Character.toUpperCase(productEl.getTextContent().charAt(0)) + productEl.getTextContent().substring(1);
                }

                NodeList descriptions = node.getElementsByTagName(PRODUCT);
                if (descriptions.getLength() > 0) {
                    Element descriptionEl = (Element) descriptions.item(0);
                    description = descriptionEl.getTextContent();
                }

                NodeList urls = node.getElementsByTagName(URL);
                if (urls.getLength() > 0) {
                    Element urlEl = (Element) urls.item(0);
                    url = urlEl.getTextContent();
                }

                NodeList prices = node.getElementsByTagName(PRICE);
                if (prices.getLength() > 0) {
                    Element priceEl = (Element) prices.item(0);
                    price = Float.parseFloat(priceEl.getTextContent());
                }

                NodeList pricesWithVAT = node.getElementsByTagName(PRICE_VAT);
                if (pricesWithVAT.getLength() > 0) {
                    Element priceWithVATEl = (Element) pricesWithVAT.item(0);
                    priceWithVAT = Float.parseFloat(priceWithVATEl.getTextContent());
                }

                NodeList images = node.getElementsByTagName(IMAGE_URL);
                if (images.getLength() > 0) {
                    Element imageEl = (Element) images.item(0);
                    imageURL = imageEl.getTextContent();
                }

                eshop.addProduct(new Product(name, description, null, eshop, url, price, priceWithVAT, imageURL));
            }

        } catch (ParserConfigurationException | SAXException ex) {
            throw new FeedParsingException(ex.getMessage());
        } catch (IOException ex) {
            throw new FeedNotFoundException();
        }
    }

}
