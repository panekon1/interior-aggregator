package cz.cvut.fel.wa2.interior.feed;

/**
 *
 * @author Ondrej Panek
 */
public class FeedNotFoundException extends Exception {

    public FeedNotFoundException() {
    }

    public FeedNotFoundException(String message) {
        super(message);
    }

}
