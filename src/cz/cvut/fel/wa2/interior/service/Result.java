package cz.cvut.fel.wa2.interior.service;

import java.util.Collection;

/**
 *
 * @author Ondrej Panek
 * @param <E>
 */
public class Result<E> {

    private Collection<E> data;
    private long count;

    public Result() {
    }

    public Result(Collection<E> data, long count) {
        this.data = data;
        this.count = count;
    }

    public Collection<E> getData() {
        return data;
    }

    public void setData(Collection<E> data) {
        this.data = data;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

}
