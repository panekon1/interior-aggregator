package cz.cvut.fel.wa2.interior.service;

import cz.cvut.fel.wa2.interior.dto.ProductDTO;
import java.util.List;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Ondrej Panek
 */
public interface ProductService {

    ProductDTO create(UriInfo uriInfo, Long eshopId, ProductDTO productDTO) throws NotExistingEntityException;

    void update(Long eshopId, Long productId, ProductDTO productDTO) throws NotExistingEntityException;

    void linkProduct(Long eshopId, Long productId, Long linkedEshopId, Long linkedProductId) throws NotExistingEntityException;

    void unlinkProduct(Long eshopId, Long productId, Long linkedEshopId, Long linkedProductId) throws NotExistingEntityException;

    void delete(Long eshopId, Long productId);

    ProductDTO findById(UriInfo uriInfo, Long eshopId, Long productId) throws NotExistingEntityException;

    Result<ProductDTO> findBy(UriInfo uriInfo, String searchString, Long categoryId, Float priceFrom, Float priceTo, String order, Integer base, Integer offset) throws NotExistingEntityException;

    Result<ProductDTO> findAll(UriInfo uriInfo, String order, Integer base, Integer offset);
    
    Result<ProductDTO> findByEshop(UriInfo uriInfo, Long eshopId, String order, Integer base, Integer offset) throws NotExistingEntityException;
    
    Result<ProductDTO> findByCategory(UriInfo uriInfo, Long categoryId, String order, Integer base, Integer offset) throws NotExistingEntityException;

    List<ProductDTO> search(UriInfo uriInfo, String searchString, String order, Integer base, Integer offset);
}
