package cz.cvut.fel.wa2.interior.service;

import com.google.appengine.api.datastore.Key;
import cz.cvut.fel.wa2.interior.dao.CategoryDAO;
import cz.cvut.fel.wa2.interior.dao.EshopDAO;
import cz.cvut.fel.wa2.interior.dao.ProductDAO;
import cz.cvut.fel.wa2.interior.dto.ProductDTO;
import cz.cvut.fel.wa2.interior.entity.Category;
import cz.cvut.fel.wa2.interior.entity.Eshop;
import cz.cvut.fel.wa2.interior.entity.Product;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private EshopDAO eshopDAO;

    @Override
    public ProductDTO create(UriInfo uriInfo, Long eshopId, ProductDTO productDTO) throws NotExistingEntityException {
        Eshop eshop = eshopDAO.findById(Eshop.class, eshopId);
        if (eshop == null) {
            throw new NotExistingEntityException("E-shop with ID " + eshopId + " does not exist.");
        }

        Category category = null;
        if (productDTO.getCategory() != null) {
            category = categoryDAO.findById(Category.class, productDTO.getCategory().getId());
            if (category == null) {
                throw new NotExistingEntityException("Category with ID " + productDTO.getCategory().getId() + " does not exist.");
            }
        }

        Product product = new Product(productDTO.getName(), productDTO.getDescription(), category, eshop, productDTO.getUrl(), productDTO.getPrice(), productDTO.getPriceWithVAT(), productDTO.getImageURL());
        eshop.addProduct(product);

        eshopDAO.persist(eshop);
        return new ProductDTO(product, uriInfo);
    }

    @Override
    public void update(Long eshopId, Long productId, ProductDTO productDTO) throws NotExistingEntityException {
        Eshop eshop = eshopDAO.findEagerlyById(Eshop.class, eshopId);
        if (eshop == null) {
            throw new NotExistingEntityException("E-shop with ID " + eshopId + " does not exist.");
        }

        Product product = productDAO.findByComplexId(Product.class, productId, Eshop.class, eshopId);
        if (product == null) {
            throw new NotExistingEntityException("Product with ID " + productId + " does not exist.");
        }

        Category category = null;
        if (productDTO.getCategory() != null) {
            category = categoryDAO.findById(Category.class, productDTO.getCategory().getId());
            if (category == null) {
                throw new NotExistingEntityException("Category with ID " + productDTO.getCategory().getId() + " does not exist.");
            }
        }

        for (Product productItem : eshop.getProducts()) {
            if (productItem.equals(product)) {
                productItem.setName(productDTO.getName());
                productItem.setDescription(productDTO.getDescription());
                productItem.setCategory(category);
                productItem.setEshop(eshop);
                productItem.setUrl(productDTO.getUrl());
                productItem.setPrice(productDTO.getPrice());
                productItem.setPriceWithVAT(productDTO.getPriceWithVAT());
                productItem.setImageURL(productDTO.getImageURL());
                System.out.println(productItem);
                break;
            }
        }
        eshopDAO.update(eshop);
    }

    @Override
    public void linkProduct(Long eshopId, Long productId, Long linkedEshopId, Long linkedProductId) throws NotExistingEntityException {

        Eshop eshop = eshopDAO.findEagerlyById(Eshop.class, eshopId);
        if (eshop == null) {
            throw new NotExistingEntityException("E-shop with ID " + eshopId + " does not exist.");
        }

        Product product = productDAO.findByComplexId(Product.class, productId, Eshop.class, eshopId);
        if (product == null) {
            throw new NotExistingEntityException("Product with ID " + productId + " does not exist.");
        }

        Product linkedProduct = productDAO.findByComplexId(Product.class, linkedProductId, Eshop.class, linkedEshopId);
        if (linkedProduct == null) {
            throw new NotExistingEntityException("Product with ID " + linkedProductId + " does not exist.");
        }

        if (eshopId.equals(linkedEshopId)) {
            for (Product productItem : eshop.getProducts()) {
                if (productItem.equals(product)) {
                    productItem.addSimilarProduct(linkedProduct);
                } else if (productItem.equals(linkedProduct)) {
                    productItem.addSimilarProduct(product);
                }
            }
            eshopDAO.update(eshop);
        } else {
            Eshop linkedEshop = eshopDAO.findEagerlyById(Eshop.class, linkedEshopId);
            if (linkedEshop == null) {
                throw new NotExistingEntityException("E-shop with ID " + linkedEshopId + " does not exist.");
            }

            for (Product productItem : eshop.getProducts()) {
                if (productItem.equals(product)) {
                    productItem.addSimilarProduct(linkedProduct);
                    break;
                }
            }

            for (Product productItem : linkedEshop.getProducts()) {
                if (productItem.equals(linkedProduct)) {
                    productItem.addSimilarProduct(product);
                    break;
                }
            }

            eshopDAO.update(eshop);
            eshopDAO.update(linkedEshop);
        }
    }

    @Override
    public void unlinkProduct(Long eshopId, Long productId, Long linkedEshopId, Long linkedProductId) throws NotExistingEntityException {
        Eshop eshop = eshopDAO.findEagerlyById(Eshop.class, eshopId);
        if (eshop == null) {
            throw new NotExistingEntityException("E-shop with ID " + eshopId + " does not exist.");
        }

        Product product = productDAO.findByComplexId(Product.class, productId, Eshop.class, eshopId);
        if (product == null) {
            throw new NotExistingEntityException("Product with ID " + productId + " does not exist.");
        }

        Product linkedProduct = productDAO.findByComplexId(Product.class, linkedProductId, Eshop.class, linkedEshopId);
        if (linkedProduct == null) {
            throw new NotExistingEntityException("Product with ID " + linkedProductId + " does not exist.");
        }

        if (eshopId.equals(linkedEshopId)) {
            for (Product productItem : eshop.getProducts()) {
                if (productItem.equals(product)) {
                    productItem.removeSimilarProduct(linkedProduct);
                } else if (productItem.equals(linkedProduct)) {
                    productItem.removeSimilarProduct(product);
                }
            }
            eshopDAO.update(eshop);
        } else {
            Eshop linkedEshop = eshopDAO.findEagerlyById(Eshop.class, linkedEshopId);
            if (linkedEshop == null) {
                throw new NotExistingEntityException("E-shop with ID " + linkedEshopId + " does not exist.");
            }

            for (Product productItem : eshop.getProducts()) {
                if (productItem.equals(product)) {
                    productItem.removeSimilarProduct(linkedProduct);
                    break;
                }
            }

            for (Product productItem : linkedEshop.getProducts()) {
                if (productItem.equals(linkedProduct)) {
                    productItem.removeSimilarProduct(product);
                    break;
                }
            }

            eshopDAO.update(eshop);
            eshopDAO.update(linkedEshop);
        }
    }

    @Override
    public void delete(Long eshopId, Long productId) {
        productDAO.removeByComplexId(Product.class, productId, Eshop.class, eshopId);
    }

    @Override
    public ProductDTO findById(UriInfo uriInfo, Long eshopId, Long productId) throws NotExistingEntityException {
        Product product = productDAO.findByComplexId(Product.class, productId, Eshop.class, eshopId);
        if (product == null) {
            throw new NotExistingEntityException("Product with ID " + productId + " does not exist.");
        }

        return new ProductDTO(product, uriInfo, true);
    }

    @Override
    public Result<ProductDTO> findBy(UriInfo uriInfo, String searchString, Long categoryId, Float priceFrom, Float priceTo, String order, Integer base, Integer offset) throws NotExistingEntityException {
        order = order != null ? order : "name";

        Category category = null;
        if (categoryId != null) {
            category = categoryDAO.findById(Category.class, categoryId);
            if (category == null) {
                throw new NotExistingEntityException("Category with ID " + category + " does not exist.");
            }
        }

        List<Product> products = productDAO.findBy(Product.class, searchString, category, priceFrom, priceTo, order, true, base, offset);
        long count = productDAO.findByCount(Product.class, searchString, category, priceFrom, priceTo);

        List<ProductDTO> productDTOs = new ArrayList<>();
        for (Product product : products) {
            productDTOs.add(new ProductDTO(product, uriInfo));
        }

        return new Result<>(productDTOs, count);
    }

    @Override
    public Result<ProductDTO> findAll(UriInfo uriInfo, String order, Integer base, Integer offset) {
        order = order != null ? order : "name";

        List<Product> products = productDAO.findAll(Product.class, order, true, base, offset);
        long count = productDAO.findAllCount(Product.class);

        List<ProductDTO> productDTOs = new ArrayList<>();
        for (Product product : products) {
            productDTOs.add(new ProductDTO(product, uriInfo));
        }

        return new Result<>(productDTOs, count);
    }

    @Override
    public Result<ProductDTO> findByEshop(UriInfo uriInfo, Long eshopId, String order, Integer base, Integer offset) throws NotExistingEntityException {
        Eshop eshop = eshopDAO.findById(Eshop.class, eshopId);
        if (eshop == null) {
            throw new NotExistingEntityException("E-shop with ID " + eshopId + " does not exist.");
        }

        order = order != null ? order : "name";
        List<Product> products = productDAO.findByProperty(Product.class, "eshop", Key.class, eshop.getKey(), order, true, base, offset);
        long count = productDAO.findByPropertyCount(Product.class, "eshop", Key.class, eshop.getKey());

        List<ProductDTO> productDTOs = new ArrayList<>();
        for (Product product : products) {
            productDTOs.add(new ProductDTO(product, uriInfo));
        }

        return new Result<>(productDTOs, count);
    }

    @Override
    public Result<ProductDTO> findByCategory(UriInfo uriInfo, Long categoryId, String order, Integer base, Integer offset) throws NotExistingEntityException {
        Category category = categoryDAO.findById(Category.class, categoryId);
        if (category == null) {
            throw new NotExistingEntityException("Category with ID " + categoryId + " does not exist.");
        }

        order = order != null ? order : "name";
        List<Product> products = productDAO.findByProperty(Product.class, "category", Key.class, category.getKey(), order, true, base, offset);
        long count = productDAO.findByPropertyCount(Product.class, "category", Key.class, category.getKey());

        List<ProductDTO> productDTOs = new ArrayList<>();
        for (Product product : products) {
            productDTOs.add(new ProductDTO(product, uriInfo));
        }

        return new Result<>(productDTOs, count);
    }

    @Override
    public List<ProductDTO> search(UriInfo uriInfo, String searchString, String order, Integer base, Integer offset) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
