package cz.cvut.fel.wa2.interior.service;

import cz.cvut.fel.wa2.interior.dto.CategoryDTO;
import java.util.List;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Ondrej Panek
 */
public interface CategoryService {

    CategoryDTO create(UriInfo uriInfo, CategoryDTO categoryDTO);

    void update(Long id, CategoryDTO categoryDTO) throws NotExistingEntityException;

    void delete(Long id);

    CategoryDTO findById(UriInfo uriInfo, Long id) throws NotExistingEntityException;

    List<CategoryDTO> findAll(UriInfo uriInfo);

}
