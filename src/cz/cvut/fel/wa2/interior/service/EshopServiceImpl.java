package cz.cvut.fel.wa2.interior.service;

import cz.cvut.fel.wa2.interior.dao.EshopDAO;
import cz.cvut.fel.wa2.interior.dto.EshopDTO;
import cz.cvut.fel.wa2.interior.entity.Eshop;
import cz.cvut.fel.wa2.interior.feed.FeedNotFoundException;
import cz.cvut.fel.wa2.interior.feed.FeedParsingException;
import cz.cvut.fel.wa2.interior.feed.FeedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EshopServiceImpl implements EshopService {

    @Autowired
    private EshopDAO eshopDAO;

    @Autowired
    private FeedReader feedReader;

    @Override
    public EshopDTO create(UriInfo uriInfo, EshopDTO eshopDTO) {
        Eshop eshop = new Eshop(eshopDTO.getName(), eshopDTO.getWeb(), eshopDTO.getFeed());

        try {
            if (eshop.getFeed() != null && !eshop.getFeed().trim().isEmpty()) {
                feedReader.parseFeed(eshop);
            }
        } catch (FeedNotFoundException | FeedParsingException ex) {
            Logger.getLogger(EshopServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            eshopDAO.persist(eshop);
        }

        return new EshopDTO(eshop, uriInfo);
    }

    @Override
    public void update(Long id, EshopDTO eshopDTO) throws NotExistingEntityException {
        Eshop eshop = eshopDAO.findById(Eshop.class, id);
        if (eshop == null) {
            throw new NotExistingEntityException("E-shop with ID " + id + " does not exist.");
        }
        eshop.setName(eshopDTO.getName());
        eshop.setWeb(eshopDTO.getWeb());
        eshop.setFeed(eshopDTO.getFeed());
        eshopDAO.update(eshop);
    }

    @Override
    public void delete(Long id) {
        eshopDAO.removeById(Eshop.class, id);
    }

    @Override
    public EshopDTO findById(UriInfo uriInfo, Long id) throws NotExistingEntityException {
        Eshop eshop = eshopDAO.findById(Eshop.class, id);
        if (eshop == null) {
            throw new NotExistingEntityException("E-shop with ID " + id + " does not exist.");
        }

        return new EshopDTO(eshop, uriInfo);
    }

    @Override
    public List<EshopDTO> findAll(UriInfo uriInfo) {
        List<Eshop> categories = eshopDAO.findAll(Eshop.class, "name", true);

        List<EshopDTO> eshopDTOs = new ArrayList<>();
        for (Eshop eshop : categories) {
            eshopDTOs.add(new EshopDTO(eshop, uriInfo));
        }
        return eshopDTOs;
    }

}
