package cz.cvut.fel.wa2.interior.service;

import cz.cvut.fel.wa2.interior.dto.EshopDTO;
import java.util.List;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Ondrej Panek
 */
public interface EshopService {

    EshopDTO create(UriInfo uriInfo, EshopDTO eshopDTO);

    void update(Long id, EshopDTO eshopDTO) throws NotExistingEntityException;

    void delete(Long id);

    EshopDTO findById(UriInfo uriInfo, Long id) throws NotExistingEntityException;

    List<EshopDTO> findAll(UriInfo uriInfo);

}
