package cz.cvut.fel.wa2.interior.service;

/**
 *
 * @author Ondrej Panek
 */
public class NotExistingEntityException extends Exception {

    public NotExistingEntityException() {
    }

    public NotExistingEntityException(String message) {
	super(message);
    }
}
