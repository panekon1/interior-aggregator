package cz.cvut.fel.wa2.interior.service;

import cz.cvut.fel.wa2.interior.dao.CategoryDAO;
import cz.cvut.fel.wa2.interior.dto.CategoryDTO;
import cz.cvut.fel.wa2.interior.entity.Category;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDAO categoryDAO;

    @Override
    public CategoryDTO create(UriInfo uriInfo, CategoryDTO categoryDTO) {
        Category category = new Category(categoryDTO.getName());
        categoryDAO.persist(category);
        return new CategoryDTO(category, uriInfo);
    }

    @Override
    public void update(Long id, CategoryDTO categoryDTO) throws NotExistingEntityException {
        Category category = categoryDAO.findById(Category.class, id);
        if (category == null) {
            throw new NotExistingEntityException("Category with ID " + id + " does not exist.");
        }
        category.setName(categoryDTO.getName());
        categoryDAO.update(category);
    }

    @Override
    public void delete(Long id) {
        categoryDAO.removeById(Category.class, id);
    }

    @Override
    public CategoryDTO findById(UriInfo uriInfo, Long id) throws NotExistingEntityException {
        Category category = categoryDAO.findById(Category.class, id);
        if (category == null) {
            throw new NotExistingEntityException("Category with ID " + id + " does not exist.");
        }
        
        return new CategoryDTO(category, uriInfo);
    }

    @Override
    public List<CategoryDTO> findAll(UriInfo uriInfo) {
        List<Category> categories = categoryDAO.findAll(Category.class, "name", true);

        List<CategoryDTO> categoryDTOs = new ArrayList<>();
        for (Category category : categories) {
            categoryDTOs.add(new CategoryDTO(category, uriInfo));
        }
        return categoryDTOs;
    }

}
