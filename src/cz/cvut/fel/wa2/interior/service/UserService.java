package cz.cvut.fel.wa2.interior.service;

import cz.cvut.fel.wa2.interior.dao.UserDAO;
import cz.cvut.fel.wa2.interior.entity.GaeUser;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ondrej Panek
 */
@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    public void create(String email) {
        GaeUser user = new GaeUser("panekon1@gmail.com");
        userDAO.persist(user);
    }

    public List<GaeUser> findAll() {
        return userDAO.findAll(GaeUser.class, "email", true);
    }

    public GaeUser findByEmail(String email) {
        List<GaeUser> users = userDAO.findByProperty(GaeUser.class, "email", String.class, email);
        return users.isEmpty() ? null : users.get(0);
    }

}
